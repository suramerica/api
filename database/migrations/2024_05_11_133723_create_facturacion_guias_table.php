<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('facturacion_guias', function (Blueprint $table) {
            $table->id();
            $table->integer('facturacionId');
            $table->integer('guiaId');
            $table->enum('tipo_cobro', ['PESO', 'VOLUMEN', 'MENOR A 1']);
            $table->float('valor_cobrado');
            $table->float('monto_cobrado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('facturacion_guias');
    }
};
