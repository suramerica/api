<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('guias', function (Blueprint $table) {
            $table->id();
            $table->integer('idUsuario');
            $table->integer('idEmpresa');
            $table->integer('idSucursal');
            $table->integer('idCliente');
            $table->integer('idDestinatario');
            $table->integer('idTasa');
            $table->integer('idCaja');
            $table->string('tipoGuia');
            $table->integer('idManifiesto');
            $table->integer('idCarga');
            $table->integer('idBulto');
            $table->string('guiaZoom');
            $table->integer('alto');
            $table->integer('ancho');
            $table->integer('largo');
            $table->float('pesoVolumetrico', 8, 5);
            $table->float('m3', 8, 3);
            $table->float('ft3', 10, 8);
            $table->float('peso', 8, 4);
            $table->float('monto', 8, 2);
            $table->enum('divisa', ['PEN', 'USD']);
            $table->string('paisOrigen');
            $table->string('paisDestino');
            $table->boolean('seguro');
            $table->float('montoSeguro', 8, 2);
            $table->boolean('entregaPP');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('guias');
    }
};
