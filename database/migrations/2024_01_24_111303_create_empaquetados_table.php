<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('empaquetados', function (Blueprint $table) {
            $table->id();
            $table->integer('idEmpresa');
            $table->string('paquete');
            $table->float('precio');
            $table->timestamps();
        });

        Schema::table('guias', function (Blueprint $table) {
            $table->boolean('paquete')->default(false);
            $table->string('paqueteNombre')->nullable();
            $table->string('paquetePrecio')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('empaquetados');
    }
};
