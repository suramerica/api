<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('guia_articulos', function (Blueprint $table) {
            $table->id();
            $table->integer('idGuia');
            $table->integer('cantidad');
            $table->float('monto', 8,2);
            $table->string('producto');
            $table->boolean('bateria');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('guia_articulos');
    }
};
