<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tasas', function (Blueprint $table) {
            $table->id();
            $table->integer('idEmpresa');
            $table->integer('idPais');
            $table->string('tasa');
            $table->enum('divisa', ['PEN', 'USD']);
            $table->enum('tipoEnvio', ['MARITIMO', 'AEREO', 'TERRESTRE']);
            $table->enum('tipoPrecio', ['FIJO', 'CALCULADO']);
            $table->float('monto', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tasas');
    }
};
