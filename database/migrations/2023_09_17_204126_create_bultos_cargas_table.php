<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('bultos_cargas', function (Blueprint $table) {
            $table->id();
            $table->integer('idCarga');
            $table->string('precinto');
            $table->float('alto');
            $table->float('ancho');
            $table->float('largo');
            $table->float('peso');
            $table->float('volumen');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bultos_cargas');
    }
};
