<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cajas', function (Blueprint $table) {
            $table->id();
            $table->integer('idEmpresa');
            $table->integer('idSucursal');
            $table->integer('idUsuario');
            $table->json('montoIniciales');
            $table->json('montoFinales');
            $table->float('gastos');
            $table->float('ingresos');
            $table->date('fechaApertura');
            $table->date('fechaCierre');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cajas');
    }
};
