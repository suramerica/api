<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('caja_registros', function (Blueprint $table) {
            $table->id();
            $table->integer('idCaja');
            $table->string('concepto');
            $table->enum('tipo', ['GASTO', 'INGRESO']);
            $table->float('monto', 8, 2);
            $table->enum('divisa', ['PEN', 'USD']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('caja_registros');
    }
};
