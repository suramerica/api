<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Categorium;
use App\Models\Producto;

class ProductoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Producto::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'idCategoria' => Categorium::factory(),
            'name' => $this->faker->name,
            'descripcion' => $this->faker->text,
            'price' => $this->faker->randomFloat(0, 0, 9999999999.),
            'stock' => $this->faker->numberBetween(-10000, 10000),
        ];
    }
}
