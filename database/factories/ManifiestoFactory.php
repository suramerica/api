<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Manifiesto;

class ManifiestoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Manifiesto::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'idCarga' => $this->faker->numberBetween(-10000, 10000),
            'idUsuario' => $this->faker->numberBetween(-10000, 10000),
            'idSucursal' => $this->faker->numberBetween(-10000, 10000),
            'idEmpresa' => $this->faker->numberBetween(-10000, 10000),
            'tipoGuia' => $this->faker->word,
            'estado' => $this->faker->word,
            'fecha' => $this->faker->date(),
        ];
    }
}
