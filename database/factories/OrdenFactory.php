<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Orden;
use App\Models\User;

class OrdenFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Orden::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'idUsuario' => User::factory(),
            'fecha' => $this->faker->date(),
            'status' => $this->faker->boolean,
            'response' => '{}',
            'monto' => $this->faker->randomFloat(2, 0, 999999.99),
            'paykuId' => $this->faker->word,
        ];
    }
}
