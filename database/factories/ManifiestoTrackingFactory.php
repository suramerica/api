<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\ManifiestoTracking;

class ManifiestoTrackingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ManifiestoTracking::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'idManifiesto' => $this->faker->numberBetween(-10000, 10000),
            'estado' => $this->faker->word,
        ];
    }
}
