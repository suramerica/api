<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\GuiaPago;

class GuiaPagoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = GuiaPago::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'idGuia' => $this->faker->numberBetween(-10000, 10000),
            'tipoPago' => $this->faker->word,
            'divisa' => $this->faker->word,
            'monto' => $this->faker->randomFloat(0, 0, 9999999999.),
        ];
    }
}
