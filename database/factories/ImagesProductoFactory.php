<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\ImagesProducto;
use App\Models\Producto;

class ImagesProductoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ImagesProducto::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'idProducto' => Producto::factory(),
            'image' => $this->faker->word,
        ];
    }
}
