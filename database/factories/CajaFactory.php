<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Caja;

class CajaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Caja::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'idEmpresa' => $this->faker->numberBetween(-10000, 10000),
            'idSucursal' => $this->faker->numberBetween(-10000, 10000),
            'idUsuario' => $this->faker->numberBetween(-10000, 10000),
            'montoIniciales' => '{}',
            'montoFinales' => '{}',
            'gastos' => $this->faker->randomFloat(0, 0, 9999999999.),
            'ingresos' => $this->faker->randomFloat(0, 0, 9999999999.),
            'fechaApertura' => $this->faker->date(),
            'fechaCierre' => $this->faker->date(),
            'status' => $this->faker->boolean,
        ];
    }
}
