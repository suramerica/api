<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Guia;

class GuiaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Guia::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'idUsuario' => $this->faker->numberBetween(-10000, 10000),
            'idEmpresa' => $this->faker->numberBetween(-10000, 10000),
            'idSucursal' => $this->faker->numberBetween(-10000, 10000),
            'idCliente' => $this->faker->numberBetween(-10000, 10000),
            'idDestinatario' => $this->faker->numberBetween(-10000, 10000),
            'idTasa' => $this->faker->numberBetween(-10000, 10000),
            'idCaja' => $this->faker->numberBetween(-10000, 10000),
            'tipoGuia' => $this->faker->word,
            'idManifiesto' => $this->faker->numberBetween(-10000, 10000),
            'idCarga' => $this->faker->numberBetween(-10000, 10000),
            'idBulto' => $this->faker->numberBetween(-10000, 10000),
            'guiaZoom' => $this->faker->word,
            'alto' => $this->faker->numberBetween(-10000, 10000),
            'ancho' => $this->faker->numberBetween(-10000, 10000),
            'largo' => $this->faker->numberBetween(-10000, 10000),
            'pesoVolumetrico' => $this->faker->randomFloat(0, 0, 9999999999.),
            'm3' => $this->faker->randomFloat(0, 0, 9999999999.),
            'ft3' => $this->faker->randomFloat(0, 0, 9999999999.),
            'peso' => $this->faker->randomFloat(0, 0, 9999999999.),
            'monto' => $this->faker->randomFloat(0, 0, 9999999999.),
            'divisa' => $this->faker->word,
            'paisOrigen' => $this->faker->word,
            'paisDestino' => $this->faker->word,
            'seguro' => $this->faker->boolean,
            'montoSeguro' => $this->faker->randomFloat(0, 0, 9999999999.),
            'entregaPP' => $this->faker->boolean,
        ];
    }
}
