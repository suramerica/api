<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\GuiaArticulo;

class GuiaArticuloFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = GuiaArticulo::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'idGuia' => $this->faker->numberBetween(-10000, 10000),
            'cantidad' => $this->faker->numberBetween(-10000, 10000),
            'monto' => $this->faker->randomFloat(0, 0, 9999999999.),
            'producto' => $this->faker->word,
            'bateria' => $this->faker->boolean,
        ];
    }
}
