<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Tasa;

class TasaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Tasa::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'idEmpresa' => $this->faker->numberBetween(-10000, 10000),
            'idPais' => $this->faker->numberBetween(-10000, 10000),
            'tasa' => $this->faker->word,
            'divisa' => $this->faker->word,
            'tipoEnvio' => $this->faker->word,
            'tipoPrecio' => $this->faker->numberBetween(-10000, 10000),
            'monto' => $this->faker->randomFloat(0, 0, 9999999999.),
        ];
    }
}
