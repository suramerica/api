<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Sucursal;

class SucursalFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Sucursal::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'idEmpresa' => $this->faker->numberBetween(-10000, 10000),
            'codigo' => $this->faker->word,
            'nombre' => $this->faker->word,
            'pais' => $this->faker->word,
            'ciudad' => $this->faker->word,
            'direccion' => $this->faker->word,
            'telefono' => $this->faker->word,
            'status' => $this->faker->numberBetween(-10000, 10000),
        ];
    }
}
