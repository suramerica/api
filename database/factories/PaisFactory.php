<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Pais;

class PaisFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Pais::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'bandera' => $this->faker->word,
            'nombre' => $this->faker->word,
            'status' => $this->faker->boolean,
        ];
    }
}
