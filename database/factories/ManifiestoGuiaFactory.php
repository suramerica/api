<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\ManifiestoGuia;

class ManifiestoGuiaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ManifiestoGuia::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'idManifiesto' => $this->faker->numberBetween(-10000, 10000),
            'idGuia' => $this->faker->numberBetween(-10000, 10000),
        ];
    }
}
