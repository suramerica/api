<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Carga;

class CargaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Carga::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'idUsuario' => $this->faker->numberBetween(-10000, 10000),
            'tipoCarga' => $this->faker->word,
            'fechaSalida' => $this->faker->date(),
            'estado' => $this->faker->word,
        ];
    }
}
