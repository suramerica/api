<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\GuiaTracking;

class GuiaTrackingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = GuiaTracking::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'idGuia' => $this->faker->numberBetween(-10000, 10000),
            'estado' => $this->faker->word,
            'descripcion' => $this->faker->word,
        ];
    }
}
