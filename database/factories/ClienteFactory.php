<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Cliente;

class ClienteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Cliente::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'idEmpresa' => $this->faker->numberBetween(-10000, 10000),
            'tipoDocumento' => $this->faker->word,
            'documento' => $this->faker->word,
            'nombre' => $this->faker->word,
            'apellido' => $this->faker->word,
            'telefono' => $this->faker->word,
            'correo' => $this->faker->word,
            'estado' => $this->faker->word,
            'ciudad' => $this->faker->word,
            'direccion' => $this->faker->word,
            'nacionalidad' => $this->faker->word,
        ];
    }
}
