<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\CajaRegistros;

class CajaRegistrosFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CajaRegistros::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'idCaja' => $this->faker->numberBetween(-10000, 10000),
            'concepto' => $this->faker->word,
            'tipo' => $this->faker->boolean,
            'monto' => $this->faker->randomFloat(0, 0, 9999999999.),
            'moneda' => $this->faker->word,
        ];
    }
}
