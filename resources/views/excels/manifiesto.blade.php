<table width="100%">
    <tr width="100%">
        <td>GUIA</td>
        <td>REMITENTE</td>
        <td>DESTINATARIO</td>
        <td>CONTENIDO</td>
        <td>PESO KG</td>
        <td>PESO VOL</td>
    </tr>
    @foreach ($guias as $guia)
        <tr>
            <td>{{$guia->sucursal->codigo}}{{$guia->id}}</td>
            <td>{{$guia->remitente->nombre}}  {{$guia->remitente->apellido}}</td>
            <td>{{$guia->destinatario->nombre}} {{$guia->destinatario->apellido}}</td>
            <td>
                @foreach ($guia->articulos as $articulo)
                    ({{$articulo->cantidad}}) {{$articulo->producto}}
                @endforeach
            </td>
            <td>{{$guia->peso}}</td>
            <td>{{$guia->pesoVolumetrico}}</td>
        </tr>
    @endforeach
</table>