<table border="1">
    <tr>
        <td colspan="3" style="text-align: center; background-color: gray;">REMITENTE</td>
        <td colspan="7" style="text-align: center; background-color: gray;">DESTINATARIO</td>
        <td colspan="10" style="text-align: center; background-color: gray;">DATOS DEL ENVIO</td>
    </tr>
    <tr>
        <td>Persona Contacto *</td>
        <td>Teléfono *</td>
        <td>Localidad *</td>
        <td>Pais Destino *</td>
        <td>Ciudad Destino *</td>
        <td>Destinatario *</td>
        <td>Persona Contacto *</td>
        <td>Teléfono *</td>
        <td>Localidad *</td>
        <td>Identificación del inmueble *</td>
        <td>Referencia</td>
        <td>Pzas *</td>
        <td>Peso Ref *</td>
        <td>Descripción de Contenido *</td>
        <td>Tipo de Envio *</td>
        <td>Alto </td>
        <td>Ancho</td>
        <td>Largo</td>
        <td>Valor de Mercancia ($) </td>
        <td>Protección Envio </td>
    </tr>
    @foreach ($guias as $guia)
        <tr>
            <td>Sur America Cargo</td>
            <td>941506196</td>
            <td>LIMA</td>
            <td>{{ $guia->paisDestino }}</td>
            <td>
                @if ($guia->entregaPP)
                    {{ $guia->oficina->ciudad }}
                @else
                    {{ $guia->destinatario->ciudad }}
                @endif
            </td>
            <td>
                {{ $guia->destinatario->nombre }} {{ $guia->destinatario->apellido }}
            </td>
            <td>
                {{ $guia->destinatario->nombre }} {{ $guia->destinatario->apellido }}
            </td>
            <td>
                {{ $guia->destinatario->telefono }}
            </td>
            <td>
                @if ($guia->entregaPP)
                    {{ $guia->oficina->direccion }}
                @else
                    {{ $guia->destinatario->direccion }}
                @endif
            </td>
            <td>
                @if ($guia->entregaPP)
                    {{ $guia->oficina->direccion }}
                @else
                    {{ $guia->destinatario->direccion }}
                @endif
            </td>
            <td>{{ $guia->sucursal->codigo }}-{{ $guia->id }}</td>
            <td>1</td>
            <td>{{ $guia->peso }}</td>
            <td>
                <?php $productos = ''; ?>
                @foreach ($guia->articulos as $articulo)
                    <?php
                    $productos .= $articulo->producto . ', ';
                    ?>
                @endforeach
                {{ substr($productos, 0, 75) }}
            </td>
            <td>M</td>
            <td>{{ $guia->alto }}</td>
            <td>{{ $guia->ancho }}</td>
            <td>{{ $guia->largo }}</td>
            <td>
                <?php
                $valorTotal = 0;
                ?>
                @foreach ($guia->articulos as $articulo)
                    <?php
                    $valor = $articulo->cantidad * $articulo->monto;
                    $valorTotal = $valorTotal + $valor;
                    ?>
                @endforeach

                {{ $valorTotal }}
            </td>
            <td></td>
        </tr>
    @endforeach
</table>
