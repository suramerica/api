<table>
    <tr>
        <th width="300px">
            <img src="{{ public_path('logo.png') }}" width="300px" alt="Logo de la empresa" class="logo">
        </th>
        <th colspan="20"  style="text-align: left;">
            <h1 style="font-size: 2rem"><b>REPORTE DE CARGA</b></h1><br>
            <strong>CARGA NUMERO: {{$guias[0]['idCarga']}}</strong>
        </th>
    </tr>
    <tr>
        <td style="font-weight: 900;">GUIA</td>
        <td style="font-weight: 900;">FECHA</td> 
        <td style="font-weight: 900;">COURIER</td>
        <td style="font-weight: 900;">DESTINO</td>
        <td style="font-weight: 900;">CONTENIDO</td>
        <td style="font-weight: 900;">REMITENTE</td>
        <td style="font-weight: 900;">DOC. REMITENTE</td>
        <td style="font-weight: 900;">DESTINATARIO</td>
        <td style="font-weight: 900;">DOC. DESTINATARIO</td>
        <td style="font-weight: 900;">TEL. DESTINATARIO</td>
        <td style="font-weight: 900;">ALTO</td>
        <td style="font-weight: 900;">ANCHO</td>
        <td style="font-weight: 900;">LARGO</td>
        <td style="font-weight: 900;">PESO</td>
        <td style="font-weight: 900;">PESO VOL.</td>
        <td style="font-weight: 900;">M3</td>
        <td style="font-weight: 900;">FT3</td>
        <td style="font-weight: 900;">FOB</td>
        <td style="font-weight: 900;">Seguro</td>
        <td style="font-weight: 900;">Delivery</td>
        <td style="font-weight: 900;">Empaquetado</td>
        <td style="font-weight: 900;">Cobro Peso</td>
        <td style="font-weight: 900;">Total</td>
    </tr>
    <?php $totalCarga = 0; ?>
    @foreach ($guias as $guia)
        <tr>
            <td>{{$guia->sucursal->codigo}}-{{$guia->id}}</td>
            <td>{{$guia->created_at}}</td>
            <td>Puerta a Puerta</td>
            <td>{{$guia->destinatario->ciudad}} - {{$guia->destinatario->direccion}} - {{$guia->destinatario->direccion}}</td>
            <td>
                <?php $fob = 0; ?>
                @foreach ($guia->articulos as $articulo)
                    ({{$articulo->cantidad}}) {{$articulo->producto}}
                    <?php $fob = $fob + ($articulo->cantidad * $articulo->monto); ?>
                @endforeach
            </td>
            <td>{{$guia->remitente->nombre}}  {{$guia->remitente->apellido}}</td>
            <td>{{$guia->remitente->tipoDocumento}}  {{$guia->remitente->documento}}</td>
            <td>{{$guia->destinatario->nombre}} {{$guia->destinatario->apellido}}</td>
            <td>{{$guia->destinatario->tipoDocumento}} {{$guia->destinatario->documento}}</td>
            <td>{{$guia->destinatario->telefono}}</td>
            <td>{{$guia->alto}}</td>
            <td>{{$guia->ancho}}</td>
            <td>{{$guia->largo}}</td>
            <td>{{$guia->peso}}</td>
            <td>{{$guia->pesoVolumetrico}}</td>
            <td>{{$guia->m3}}</td>
            <td>{{$guia->ft3}}</td>
            <td>{{$fob}}</td>
            <td>{{$guia->montoSeguro}} {{$guia->divisa}}</td>
            <td>{{$guia->paquetePrecio}} {{$guia->divisa}}</td>
            <td>{{$guia->montoDelivery}} {{$guia->divisa}}</td>
            <td>{{$guia->monto - $guia->montoSeguro - $guia->paquetePrecio - $guia->montoDelivery}} {{$guia->divisa}}</td>
            <td>{{$guia->monto}} {{$guia->divisa}}</td>
            <?php $totalCarga = $totalCarga + $guia->monto; ?>
        </tr>
    @endforeach
</table>
