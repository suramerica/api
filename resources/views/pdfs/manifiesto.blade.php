<div style="text-align: center;">
    <h1>Manifiesto: {{ $manifiesto->id }}</h1>
    {!! DNS1D::getBarcodeHTML($manifiesto->id, 'C128', 2, 25) !!}
    <table style="width: 100%">
        <tr style="width: 100%">
            <td style="width: 50%">Empresa: {{ $manifiesto->empresa->nombre }}</td>
            <td style="width: 50%; text-align: right">Sucursal: {{ $manifiesto->sucursal->nombre }}</td>
            <td style="width: 50%; text-align: right">Tipo Manifiesto: {{ $manifiesto->tipoGuia }}</td>
        </tr>
        <tr style="width: 100%">
            <td style="width: 50%">Creado Por: {{ $manifiesto->creador->name }} (Usuario:
                {{ $manifiesto->creador->email }})</td>
            <td style="width: 50%; text-align: right">Fecha: {{ $manifiesto->created_at }}</td>
            <td style="width: 50%; text-align: right">Peso Total: {{ $peso }}</td>
        </tr>
    </table>

    <table style="width: 100%; margin-top: 20px">
        <tr style="width: 100%; background-color: gray; color: white">
            <td style="padding-top: 5px; padding-bottom: 5px">Nº</td>
            <td style="padding-top: 5px; padding-bottom: 5px">GUIA</td>
            <td style="padding-top: 5px; padding-bottom: 5px">DESTINATARIO</td>
            <td style="padding-top: 5px; padding-bottom: 5px">DOCUMENTO</td>
            <td style="padding-top: 5px; padding-bottom: 5px">KG</td>
            <td style="padding-top: 5px; padding-bottom: 5px">VOL</td>
            <td style="padding-top: 5px; padding-bottom: 5px">M3</td>
            <td style="padding-top: 5px; padding-bottom: 5px">FT3</td>
            <td style="padding-top: 5px; padding-bottom: 5px">FOB</td>
        </tr>
        <?php $init = 1; ?>
        @foreach ($manifiesto->guias as $guia)
            <tr>
                <td>{{ $init++ }}</td>
                <td>{{ $manifiesto->sucursal->codigo }}-{{ $guia->id }}</td>
                <td>{{ $guia->destinatario->nombre }} {{ $guia->destinatario->apellido }}</td>
                <td>{{ $guia->destinatario->tipoDocumento }} {{ $guia->destinatario->documento }}</td>
                <td style="text-align: center">{{ $guia->peso }} KG</td>
                <td style="text-align: center">{{ $guia->pesoVolumetrico }}</td>
                <td style="text-align: center">{{ $guia->m3 }}</td>
                <td style="text-align: center">{{ $guia->ft3 }}</td>
                <td>
                    <?php
                    $valorTotal = 0;
                    ?>
                    @foreach ($guia->articulos as $articulo)
                        <?php
                        $valor = $articulo->cantidad * $articulo->monto;
                        $valorTotal = $valorTotal + $valor;
                        ?>
                    @endforeach

                    {{ $valorTotal }} USD
                </td>
            </tr>
        @endforeach
    </table>

    <table style="width: 100%; margin-top: 20px">
        <tr style="width: 100%">
            <td style="width: 33%">_________________________________ <br>
                Entregado Por:
            </td>
            <td style="width: 33%">_________________________________ <br>
                Recibido Por:
            </td>
            <td style="width: 33%">_________________________________ <br>
                Aprobado Por:
            </td>
        </tr>
    </table>
</div>
