<html>

<head>
    <style>
        @page {
            margin: 0cm 1cm;
            font-size: 8
        }

        body {
            margin-top: 2cm;
            margin-left: 2cm;
            margin-right: 2cm;
            margin-bottom: 2cm;
        }

        header {
            position: fixed;
            top: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;
            color: black;
            text-align: center;
            line-height: 1.5cm;
        }

        .text-titulo {
            width: 100%;
            font-size: 2rem;
            font-weight: 700;
            text-align: center
        }

        .text-subtitulo {
            width: 100%;
            font-size: 10;
            text-align: center;
            text-transform: uppercase;
        }

        .hr {
            width: 100%
        }

        .row {
            width: 100;
        }

        .text-right {
            width: 100%;
            text-align: right !important;
        }

        .text-uppercase {
            text-transform: uppercase
        }

        .observacion-titulo {
            width: 100%;
            font-size: 10;
            text-align: center;
            text-transform: uppercase;
            font-weight: 600;
            margin-top: 1rem;
            text-decoration: underline
        }

        .observacion-contenido {
            text-align: justify;
            margin-top: 0.5rem
        }

        .firma {
            width: 100%;
            text-align: center;
            font-weight: 600;
            font-size: 10;
            margin-top: 5rem
        }


        .titulo2 {
            font-weight: 800;
            font-size: 3rem;
            width: 100%;
            text-align: center
        }

        .subtitulo2 {
            font-weight: 700;
            font-size: 2rem;
            width: 100%;
            text-align: center
        }

        .description {
            width: 100%;
            text-align: justify !important;
            padding-top: 1rem
        }

        tr.border_bottom td {
            border-bottom: 1px solid black;
        }

        .img-drogas{
            width: 100%;
            margin-top: 2rem;
        }
    </style>
</head>

<body>
    <main>
        <div class="titulo2">PACKING LIST - GUIA {{$guia->sucursal->codigo}}-{{$guia->id}}</div>

        <table style="width: 100%; margin-top: 1rem">
            <tr style="width: 100%; background-color: #c8c8c8; border-bottom: solid 1px black">
                <td>Height</td>
                <td>Width</td>
                <td>Long</td>
                <td>Weight</td>
                <td>Vol</td>
                <td>Ft3</td>
                <td>M3</td>
            </tr>
            <tr>
                <td>{{$guia->alto}}</td>
                <td>{{$guia->ancho}}</td>
                <td>{{$guia->largo}}</td>
                <td>{{$guia->peso}}</td>
                <td>{{$guia->pesoVolumetrico}}</td>
                <td>{{$guia->ft3}}</td>
                <td>{{$guia->m3}}</td>
            </tr>
        </table>
        <table style="width: 100%; margin-top: 1rem">
            <tr style="width: 100%; background-color: #c8c8c8; border-bottom: solid 1px black">
                <td style="width: 5%; padding: 1 0 1 2">
                    <b>N#</b>
                </td>
                <td style="width: 60%; padding: 1 0 1 2">
                    <b>DESCRIPTION</b>
                </td>
                <td style="width: 20%; padding: 1 0 1 0">
                    <b>QUANTITY (SETS)</b>
                </td>
            </tr>
            <?php $valor = 0;
            $suma = 1; ?>
            @foreach ($guia->articulos as $articulo)
                <tr class="border_bottom">
                    <td>{{$suma}}</td>
                    <td>{{ $articulo->producto }}</td>
                    <td>{{ $articulo->cantidad }}</td>
                </tr>
                <?php $suma = $suma + 1; ?>
            @endforeach
           
        </table>

    </main>


</body>

</html>
