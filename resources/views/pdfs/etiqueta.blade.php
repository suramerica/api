<style>
    .logo {
        width: 150px;
    }

    .qr {
        position: absolute;
        top: 0px;
        right: 10px;
        padding: 3px;
        border: solid 1px black;
        border-radius: 10px;
    }

    .barcode {
        padding: 3px;
        border: solid 1px black;
        border-radius: 10px;
    }

    .codigo {
        position: relative;
        font-size: 40px;
        font-weight: 700;
        margin-top: 25px
    }

    .fecha {
        margin-top: -1rem;
        font-size: 12px
    }

    .box {
        display: flex;
        flex-direction: row;
    }

    .titulo {
        font-style: 20px;
        font-weight: 700;
    }

    .informacion {
        font-size: 12px;
        width: 70%;
    }

    .casilla {
        width: 100%;
        border: solid 1px black;
        padding: 3px;
        border-radius: 10px;
        margin-bottom: 10px;
    }

    .centrar {
        text-align: center;
        position: absolute;
        margin-top: 1rem;
        left: 50%;
        /* Centrar horizontalmente */
        transform: translate(-50%, -50%);
        /* Ajustar para centrar correctamente */
    }
</style>
{{-- <img src="https://suramericacargo.com/images/logo.png" class="logo" alt=""> <br> --}}
<span class="titulo">{{ $empresa->nombre }}</span> <br>
<div class="informacion">
    ({{ $guia->sucursal->codigo }}) - {{ $guia->sucursal->nombre }} <br>
    {{ $guia->sucursal->direccion }} <br>
    Telefono: {{ $guia->sucursal->telefono }} <br>
</div>

<p class="codigo">{{ $guia->sucursal->codigo }}-{{ $guia->id }}</p>


@if($empresa->id != 9)
<img src="https://api.qrserver.com/v1/create-qr-code/?size=120x120&data=https://suramericacargo.com/tracking/{{ $guia->id }}" class="qr">
@endif

<div class="casilla">
    <strong>Destinatario: </strong> {{ $guia->destinatario->nombre }} {{ $guia->destinatario->apellido }}
</div>

<div class="casilla">
    <strong>Pais: </strong> {{ $guia->paisDestino }}
</div>

<div class="casilla">
    <strong>Peso: </strong> {{ $guia->peso }} KG
</div>

<div class="casilla">
    <strong>Tipo de Envio: </strong> {{ $guia->tipoGuia }}
</div>

<div class="centrar">
    {!! DNS1D::getBarcodeHTML($guia->id, "C128", 2, 25) !!}
</div>

{{-- <img src="{{assets(DNS1D::getBarcodeSVG('4445645656', 'PHARMA2T'))}}" alt=""> --}}