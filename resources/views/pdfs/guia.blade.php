<html>

<head>
    <style>
        @page {
            margin: 0cm 1cm;
            font-size: 8
        }

        body {
            margin-top: 2cm;
            margin-left: 2cm;
            margin-right: 2cm;
            margin-bottom: 2cm;
        }

        header {
            position: fixed;
            top: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;
            color: black;
            text-align: center;
            line-height: 1.5cm;
        }

        .text-titulo {
            width: 100%;
            font-size: 2rem;
            font-weight: 700;
            text-align: center
        }

        .text-subtitulo {
            width: 100%;
            font-size: 10;
            text-align: center;
            text-transform: uppercase;
        }

        .hr {
            width: 100%
        }

        .row {
            width: 100;
        }

        .text-right {
            width: 100%;
            text-align: right !important;
        }

        .text-uppercase {
            text-transform: uppercase
        }

        .observacion-titulo {
            width: 100%;
            font-size: 10;
            text-align: center;
            text-transform: uppercase;
            font-weight: 600;
            margin-top: 1rem;
            text-decoration: underline
        }

        .observacion-contenido {
            text-align: justify;
            margin-top: 0.5rem
        }

        .firma {
            width: 100%;
            text-align: center;
            font-weight: 600;
            font-size: 10;
            margin-top: 5rem
        }


        .titulo2 {
            font-weight: 800;
            font-size: 3rem;
            width: 100%;
            text-align: center
        }

        .subtitulo2 {
            font-weight: 700;
            font-size: 2rem;
            width: 100%;
            text-align: center
        }

        .description {
            width: 100%;
            text-align: justify !important;
            padding-top: 1rem
        }

        tr.border_bottom td {
            border-bottom: 1px solid black;
        }

        .img-drogas {
            width: 100%;
            margin-top: 2rem;
        }
    </style>
</head>

<body>
    <header>
        <table style="width: 100%">
            <tr style="width: 100%">
                <td style="width: 90%; font-size: 8">Emision Guia</td>
                <td style="width: 20%; text-align:left!important; font-size: 8">{{ $guia->created_at }}</td>
            </tr>
        </table>
    </header>

    <main>
        <div class="text-titulo">
            Guia: {{ $guia->sucursal->codigo . '-' . $guia->id }}
        </div>
        <div class="text-subtitulo">
            (OFICINA: {{ $guia->sucursal->codigo }} - {{ $guia->sucursal->nombre }})
        </div>

        <hr class="hr">

        <table style="width: 100%">
            <tr style="width: 100%">
                <td style="width: 50%">Peso En Kilos: {{ $guia->peso }} KG</td>
                <td style="width: 50%">
                    <div class="text-right">Peso Volumetrico: {{ $guia->pesoVolumetrico }}</div>
                </td>
            </tr>

            <tr style="width: 100%">
                <td style="width: 50%">Medidas: {{ $guia->alto }}CM {{ $guia->ancho }}CM {{ $guia->largo }}CM</td>
                <td style="width: 50%">
                    <div class="text-right">Medidas en Pulgadas: {{ number_format($guia->alto / 2.54, 2) }}"x
                        {{ number_format($guia->ancho / 2.54, 2) }}\"x {{ number_format($guia->largo / 2.54, 2) }}"
                    </div>
                </td>
            </tr>

            <tr style="width: 100%">
                <td style="width: 50%">Pie Cubico: {{ number_format($guia->ft3, 4) }}</td>
                <td style="width: 50%">
                    <div class="text-right">M3: {{ number_format($guia->m3, 3) }}</div>
                </td>
            </tr>
        </table>

        <hr class="hr">

        <table style="width: 100%">
            <tr style="width: 100%">
                <td style="width: 50%" class="">Tipo de Envio: <span class="text-uppercase">{{ $guia->tipoGuia }}
                    </span>
                </td>
            </tr>
            <tr style="width: 100%">
                <td style="width: 50%" class="">Pais Envio: <span class="text-uppercase">{{ $guia->paisOrigen }}
                    </span>
                </td>
                <td style="width: 50%">
                    <div class="text-right ">Pais Destino: <span class="text-uppercase">{{ $guia->paisDestino }}</span>
                    </div>
                </td>
            </tr>

            <tr style="width: 100%">
                <td style="width: 100%">Remitente: <span class="text-uppercase ">{{ $guia->remitente->nombre }}
                        {{ $guia->remitente->apellido }}</span>
                </td>
            </tr>

            <tr style="width: 100%">
                <td style="width: 50%">Tipo Documento:
                    {{ $guia->remitente->tipoDocumento }}
                </td>
                <td style="width: 50%">
                    <div class="text-right">Numero de Documento: {{ $guia->remitente->documento }}</div>
                </td>
            </tr>

            <tr style="width: 100%">
                <td style="width: 70%">Destinatario: <span class="text-uppercase ">{{ $guia->destinatario->nombre }}
                        {{ $guia->destinatario->apellido }}</span>
                </td>
                <td style="width: 30%">
                    <div class="text-right">Telefono: {{ $guia->destinatario->telefono }}</div>
                </td>
            </tr>

            <tr style="width: 100%">
                <td style="width: 50%">Tipo Documento:
                    {{ $guia->destinatario->tipoDocumento }}
                </td>
                <td style="width: 50%">
                    <div class="text-right">Numero de Documento: {{ $guia->destinatario->documento }}</div>
                </td>
            </tr>
        </table>

        <hr class="hr">
        <table style="width: 100%">
            <tr style="width: 100%">
                <td style="width: 50%" class="">
                    Identificador de carga:
                    @if ($guia->idCarga != null)
                        {{ $guia->idCarga }}
                    @else
                        NO ASIGNADA
                    @endif
                </td>
                <td style="width: 50%">
                    <div class="text-right ">
                        Identificador de Manifiesto:
                        @if ($guia->idManifiesto != null)
                            {{ $guia->idManifiesto }}
                        @else
                            NO ASIGNADO
                        @endif
                    </div>
                </td>
            </tr>
            <tr style="width: 100%">
                <td style="width: 50%" class="">Courier: <span class="text-uppercase">

                        @if ($guia->entregaPP)
                            ZOOM
                        @else
                            Entrega Puerta a Puerta
                        @endif


                    </span>
                </td>
                <td style="width: 50%">
                    <div class="text-right ">Tipo de Entrega:
                        @if ($guia->entregaPP)
                            Courier
                        @else
                            Entrega Puerta a Puerta
                        @endif
                    </div>
                </td>
            </tr>
        </table>

        <p>
            Direccion:<br>
            @if ($guia->entregaPP)
                <b>Ciudad:</b> {{ $guia->oficina->ciudad }} <br>
                <b>Estado:</b> {{ $guia->oficina->estado }} <br>
                <b>Direccion:</b> {{ $guia->oficina->direccion }} <br>
                <b>Oficina:</b> {{ $guia->oficina->nombre }} <br>
            @else
                <b>Ciudad:</b> {{ $guia->destinatario->ciudad }} <br>
                <b>Estado:</b> {{ $guia->destinatario->estado }} <br>
                <b>Direccion:</b> {{ $guia->destinatario->direccion }} <br>
            @endif
        </p>

        <hr class="hr">
        <table style="width: 100%">
            <tr style="width: 100%; background-color: gray; border-bottom: solid 1px black">
                <td style="width: 80%; padding: 3 0 3 5">
                    <b>RESUMEN</b>
                </td>
                <td style="width: 20%; padding: 3 0 3 0">
                    <b>TOTAL</b>
                </td>
            </tr>

            <tr style="width: 100%">
                <td style="width: 80%; padding: 3 0 3 5">
                    <div class="text-uppercase">
                        @foreach ($guia->articulos as $articulo)
                            <b>
                                ({{ $articulo->cantidad }})
                                {{ $articulo->producto }}
                            </b>
                        @endforeach
                    </div>

                </td>
                <td style="width: 20%; padding: 3 0 3 0">
                    <?php
                    $valorTotal = 0;
                    ?>
                    @foreach ($guia->articulos as $articulo)
                        <?php
                        $valor = $articulo->cantidad * $articulo->monto;
                        $valorTotal = $valorTotal + $valor;
                        ?>
                    @endforeach

                    {{ $valorTotal }}
                </td>
            </tr>
        </table>

        <div class="observacion-titulo">OBSERVACION IMPORTANTE</div>
        <div class="observacion-contenido">Certifico bajo juramento que el contenido del presente envío entregado a
            {{ $guia->sucursal->codigo }}-{{ $empresa->nombre }}. Se ajusta a lo declarado en la guía
            y me hago responsable, ante las autoridades nacionales y extranjeras por el contenido y valor declarado.
            Este envío cumple los parámetros
            aduaneros del país destino. Adicionalmente certifico que el envió no tiene dinero, valores negociables, ni
            objetos de prohibido transporte,
            según las normas internacionales y legislación aplicable en el país destino u origen. Puede Existir retraso
            en la llegada en la cual acepto.</div>



        <div class="firma">
            _________________ <br> Firma Remitente
        </div>
    </main>



    <div style="page-break-after:always;"></div>

    <main>
        <div class="titulo2">ANEXO 1</div>
        <div class="subtitulo2">DECLARACION JURADA DE VALOR</div>

        <div class="description">
            Yo <span class="text-uppercase">{{ $guia->remitente->nombre }} {{ $guia->remitente->apellido }}</span> de
            nacionalidad {{ $guia->remitente->nacionalidad }} , con documento de identidad N°
            {{ $guia->remitente->documento }}, domiciliado
            en {{ $guia->remitente->direccion }} - {{ $guia->remitente->ciudad }} - {{ $guia->remitente->estado }},
            en
            merito a la Ley del Procedimiento Administrativo General Ley N° 27444, declaro el valor FOB estimado de la
            mercancia, asi como de los datos siguientes: GUIA: {{ $guia->sucursal->codigo }}-{{ $guia->id }}
        </div>

        <table style="width: 100%; margin-top: 1rem">
            <tr style="width: 100%; background-color: gray; border-bottom: solid 1px black">
                <td style="width: 60%; padding: 3 0 3 5">
                    <b>Descr. y Caract. de la Mercancia</b>
                </td>
                <td style="width: 10%; padding: 3 0 3 0">
                    <b>Cant</b>
                </td>
                <td style="width: 10%; padding: 3 0 3 0">
                    <b>U. Med</b>
                </td>
                <td style="width: 10%; padding: 3 0 3 0">
                    <b>V. Unit</b>
                </td>
                <td style="width: 10%; padding: 3 0 3 0">
                    <b>V. FOB</b>
                </td>

            </tr>
            <?php $valor = 0;
            $suma = 0; ?>
            @foreach ($guia->articulos as $articulo)
                <tr class="border_bottom">
                    <td>{{ $articulo->producto }}</td>
                    <td>{{ $articulo->cantidad }}</td>
                    <td>UNIDAD</td>
                    <td>{{ $articulo->monto }}</td>
                    <td>{{ $valor = $articulo->cantidad * $articulo->monto }}</td>
                </tr>
                <?php $suma = $suma + $valor; ?>
            @endforeach
            <tr class="border_bottom">
                <td></td>
                <td></td>
                <td></td>
                <td>TOTAL</td>
                <td>{{ $suma }}</td>
            </tr>
        </table>

        <div class="description">
            Declaro bajo juramento que los presentes datos obedecen a la verdad, sometiendome a las sanciones
            administrativas, civiles y penales que
            correspondan en caso de falsedad de los mismo
        </div>

        <div class="firma">
            Lima {{ date('d-m-Y') }} <br><br><br>
            _________________ <br> Firma Remitente
        </div>
    </main>


    <div style="page-break-after:always;"></div>

    <main>
        <div class="titulo2">ANEXO 2</div>
        <div class="subtitulo2">CARTA ANTI-DROGA</div>

        <div class="description">

            Yo <span class="text-uppercase">{{ $guia->remitente->nombre }} {{ $guia->remitente->apellido }}</span> ,
            con
            {{ $guia->remitente->tipoDocumento }} Nro. {{ $guia->remitente->documento }} de nacionalidad
            {{ $guia->remitente->nacionalidad }}
            manifiesto que la encomienda
            la cual decido enviar a traves de la empresa {{ $guia->sucursal->codigo }}-{{ $empresa->nombre }} bajo
            numero de guia
            {{ $guia->sucursal->codigo }}-{{ $guia->id }} declaro bajo fe de juramento que no se transporta ningun
            tipo de sustancia psicotropicas o estupefacientes señaladas en la Ley Organica de Drogas, asumiendo toda la
            responsabilidad del contenido de estos efectos, objetos, documentos u otros tipo de producto
        </div>

        <img src="https://apiapp.suramericacargo.com/img//tabla-antidroga.png" class="img-drogas"
            alt="">
    </main>
</body>

</html>
