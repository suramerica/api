<style>
    /** Define the margins of your page **/
    @page {
        margin: 10 0 0 0;
    }

    header {
        position: fixed;
        top: -110px;
        left: 0px;
        right: 0px;
        height: 50px;

        color: white;
        text-align: center;
        line-height: 35px;
    }

    .titulos {
        width: 100%;
        background-color: #b9b9b9;
        font-weight: 600;
        text-align: center
    }

    .contenidos {
        width: 90%;
        margin-left: 5%;
        font-size: 10;
        text-align: left
    }

    .contenidosTerminos {
        width: 90%;
        margin-left: 5%;
        font-size: 8;
        text-align: justify
    }

    .contenidosTotal {
        width: 90%;
        margin-left: 5%;
        font-size: 19;
        text-align: center;
        font-weight: 700
    }

    .contenidosTotal {
        width: 90%;
        margin-left: 5%;
        font-size: 13;
        text-align: center;
        font-weight: 700
    }

    .text-uppercase {
        text-transform: uppercase
    }
</style>
</head>

<body>
    <main style="text-align: center">
        @if($empresa->id != 11)
            <h3 class="text-uppercase">{{ $empresa->nombre }}</h3>
        @endif
        ({{ $guia->sucursal->codigo }}) - {{ $guia->sucursal->nombre }} <br>
        {{ $guia->sucursal->direccion }} <br>
        Telefono: {{ $guia->sucursal->telefono }} <br>

        <table style="width: 100%">
            <tr style="width: 100%">
                <td style="width: 50%; ">
                    <h3 style="padding-left: 1rem">Tracking:</h3>
                </td>
                <td style="width: 50%; text-align: right; padding-right: 1rem">
                    <h3>{{ $guia->sucursal->codigo }}-{{ $guia->id }}</h3>
                </td>
            </tr>
            <tr style="width: 100%">
                <td style="width: 30%; ">
                    <span style="padding-left: 1rem">Fecha:</span>
                </td>
                <td style="width: 70%; text-align: right; padding-right: 1rem">
                    <span style="padding-left: 1rem">{{ $guia->created_at }}</span>
                </td>
            </tr>
        </table>
        <br>
        <div class="titulos">
            REMITENTE
        </div>

        <div class="contenidos">
            <b>Nombre: {{ $guia->remitente->nombre }} {{ $guia->remitente->apellido }}</b> <br>
            <b>Nro. Documento: {{ $guia->remitente->documento }}</b> <br>
            <b>Telefono: {{ $guia->remitente->telefono }}</b> <br>
        </div>

        <br>
        <div class="titulos">
            DESTINTARIO
        </div>
        <div class="contenidos">
            <b>Nombre: {{ $guia->destinatario->nombre }} {{ $guia->destinatario->apellido }}</b> <br>
            <b>Nro. Documento: {{ $guia->destinatario->documento }}</b> <br>
            <b>Telefono: {{ $guia->destinatario->telefono }}</b> <br>
        </div>

        <br>
        <div class="titulos">
            @if ($guia->entregaPP)
            RECOJO EN COURIER
            @else
            ENTREGA PUERTA
            @endif

        </div>
        <div class="contenidos">
            <b>Pais Envio: </b> <span class="text-uppercase">{{ $guia->paisOrigen }}
            </span>
            <div class="text-right "><b>Pais Destino: </b> <span class="text-uppercase">{{ $guia->paisDestino }}</span>
            </div>
            @if ($guia->entregaPP)
            <b>Ciudad:</b> {{ $guia->oficina->ciudad }} <br>
            <b>Estado:</b> {{ $guia->oficina->estado }} <br>
            <b>Direccion:</b> {{ $guia->oficina->direccion }} <br>
            @else
            <b>Ciudad:</b> {{ $guia->destinatario->ciudad }} <br>
            <b>Estado:</b> {{ $guia->destinatario->estado }} <br>
            <b>Direccion:</b> {{ $guia->destinatario->direccion }} <br>
            @endif

        </div>
        <br>
        <div class="titulos">
            PAQUETE
        </div>
        <div class="contenidos">
            <b>Tipo Envio:</b> <span class="text-uppercase">{{ $guia->tipoGuia }}</span><br>
            <b>Peso KG:</b> {{ $guia->peso }} <br>
            <b>Peso Vol:</b> {{ $guia->pesoVolumetrico }} <br>
            <b>Dimensiones:</b> {{ $guia->alto }}cm x {{ $guia->ancho }}cm x {{ $guia->largo }}cm <br>
        </div>

        @if ($guia->montoSeguro != 0)
        <br>
        <div class="titulos">
            SEGURO ENVIO
        </div>
        <div class="contenidosTotal">
            {{ $guia->montoSeguro }} {{$guia->divisa}}
        </div>
        @else
        <br>
        <div class="titulos">
            SEGURO ENVIO
        </div>
        <div class="contenidosTotal2">
            ENVIO NO ASEGURADO
        </div>
        @endif

        @if ($guia->delivery != 0)
        <br>
        <div class="titulos">
            DELIVERY
        </div>
        <div class="contenidosTotal">
            {{ $guia->montoDelivery }} {{$guia->divisa}}
        </div>
        @endif

        @if ($guia->paquete != 0)
        <br>
        <div class="titulos">
            PAQUETE ADICIONAL
        </div>
        <div class="contenidosTotal">
            {{ $guia->paqueteNombre }} <br>
            {{ $guia->paquetePrecio }} {{$guia->divisa}}
        </div>
        @endif

        <br>

        <div class="titulos">
            COSTO ENVIO
        </div>
        <div class="contenidosTotal">
            {{ $guia->monto }} {{$guia->divisa}}
        </div>

        <br>
        <div class="titulos">
            Terminos y Condiciones
        </div>

        <div class="contenidosTerminos">
            ENTIENDO Y ACEPTO QUE "{{ $empresa->nombre }}",
            NO ASUMIRÁ RESPONSABILIDAD ALGUNA POR LOS
            ENVIOS DECOMISADOS BIEN SEA POR CONTENER
            OBJETOS PROHIBIDOS O SOMETIDOS A DERECHO DE
            ADUANA O CONFISCADO POR LAS AUTORIDADES. Asi
            mismo declaro conocer que este servicio tiene limitaciones
            impuestos por razones de conveniencia general en defensa
            moral y pública de seguridad nacional, defensa del tesoro
            público y también por razones de interés del propio servicio y
            de sus funciones. De acuerdo con el principio anterior, certifico
            que no estoy enviando ningúno de los siguientes articulos. *
            Objetos cuya admisión o circulación esté prohibida en el país
            destino. * Dinero en efectivo y otros objetos de valor como
            (moneda, oro, platino, plata, piedras preciosas). * Materiales
            explosivos, informales, virus,agentes irritables o peligrosos. *
            Todo aquello que los convenios o acuerdos internacionales
            consagran como prohibida circulación. Dicho esto también
            declaro haber sido notificado que "Todos los envíos
            internacionales están sujetos a revisión por parte de las
            autoridades en los países de origen, destino y transito y pueden
            llegar a generar pagos de impuestos en el país de destino y/o
            demorarse por seguridad, por lo tanto, me comprometo a
            presentar y pagar ante las autoridades correspondientes los
            impuestos que pudiense generar en las aduanas nacionales o
            coordinar con el destinatario para el pago de los impuestos
            aduanales en el país destino en caso que así lo requieran las las
            autoridades correspondientes"
        </div>
        <br>
        @if($empresa->id != 9 && $empresa->id != 11)
            <img src="https://api.qrserver.com/v1/create-qr-code/?size=100x100&data=https://suramericacargo.com/tracking/{{$guia->id}}" alt="">
        @endif
    </main>
</body>

</html>
