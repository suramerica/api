<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
    use HasFactory;

    protected $fillable = [
        'idEmpresa',
        'codigo',
        'nombre',
        'pais',
        'ciudad',
        'direccion',
        'telefono',
        'status',
    ];


    protected $casts = [
        'id' => 'integer',
    ];

    public function guias()
    {
        return $this->hasMany(Guia::class, 'idSucursal', 'id');
    }
}
