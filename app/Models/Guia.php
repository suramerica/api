<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Guia extends Model
{
    use HasFactory;

    protected $fillable = [
        'idUsuario',
        'idEmpresa',
        'idSucursal',
        'idCliente',
        'idDestinatario',
        'idTasa',
        'idCaja',
        'tipoGuia',
        'idManifiesto',
        'idCarga',
        'idBulto',
        'guiaZoom',
        'alto',
        'ancho',
        'largo',
        'pesoVolumetrico',
        'm3',
        'ft3',
        'peso',
        'monto',
        'divisa',
        'paisOrigen',
        'paisDestino',
        'seguro',
        'montoSeguro',
        'delivery',
        'montoDelivery',
        'entregaPP',
        'paquete',
        'paqueteNombre',
        'paquetePrecio',
    ];

    protected $casts = [
        'id' => 'integer',
        'pesoVolumetrico' => 'float',
        'm3' => 'float',
        'ft3' => 'float',
        'peso' => 'float',
        'monto' => 'float',
        'seguro' => 'boolean',
        'montoSeguro' => 'float',
        'entregaPP' => 'boolean',
        'created_at' => 'date:d-m-Y'
    ];

    public function destinatario()
    {
        return $this->hasOne(Destinatario::class, 'id', 'idDestinatario');
    }

    public function remitente()
    {
        return $this->hasOne(Cliente::class, 'id', 'idCliente');
    }

    public function sucursal()
    {
        return $this->hasOne(Sucursal::class, 'id', 'idSucursal');
    }

    public function trackings()
    {
        return $this->hasMany(GuiaTracking::class, 'idGuia', 'id');
    }

    public function articulos()
    {
        return $this->hasMany(GuiaArticulo::class, 'idGuia', 'id');
    }

    public function oficina()
    {
        return $this->hasOne(Courier::class, 'id', 'idOficinaCourier');
    }

    public function empresa()
    {
        return $this->hasOne(Empresa::class, 'id', 'idEmpresa');
    }
}
