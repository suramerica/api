<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Caja extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idEmpresa',
        'idSucursal',
        'idUsuario',
        'montoIniciales',
        'montoFinales',
        'gastos',
        'ingresos',
        'fechaApertura',
        'fechaCierre',
        'status',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'montoIniciales' => 'array',
        'montoFinales' => 'array',
        'gastos' => 'float',
        'ingresos' => 'float',
        'fechaApertura' => 'date',
        'fechaCierre' => 'date',
        'status' => 'boolean',
    ];
}
