<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Manifiesto extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idCarga',
        'idUsuario',
        'idSucursal',
        'idEmpresa',
        'tipoGuia',
        'estado',
        'fecha',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'fecha' => 'date',
    ];

    public function creador()
    {
        return $this->hasOne(User::class, 'id', 'idUsuario');
    }

    public function sucursal()
    {
        return $this->hasOne(Sucursal::class, 'id', 'idSucursal');
    }

    public function empresa()
    {
        return $this->hasOne(Empresa::class, 'id', 'idEmpresa');
    }

    public function guias()
    {
        return $this->hasMany(Guia::class, 'idManifiesto', 'id');
    }

    public function guiasScaneadas()
    {
        return $this->hasMany(Guia::class, 'idManifiesto', 'id')->where('escaneada', 1);
    }
}
