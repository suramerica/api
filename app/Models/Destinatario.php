<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Destinatario extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idCliente',
        'idEmpresa',
        'tipoDocumento',
        'documento',
        'nombre',
        'apellido',
        'telefono',
        'estado',
        'ciudad',
        'direccion',
        'nacionalidad',
        'pais'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
    ];
}
