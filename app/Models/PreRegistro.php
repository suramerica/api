<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PreRegistro extends Model
{
    use HasFactory;

    protected $fillable = [
        'idEmpresa',
        'idSucursal',
        'idCliente',
        'idDestinatario',
        'idCourier',
    ];  


    public function destinatario()
    {
        return $this->hasOne(Destinatario::class, 'id', 'idDestinatario');
    }

    public function remitente()
    {
        return $this->hasOne(Cliente::class, 'id', 'idCliente');
    }

    public function oficina()
    {
        return $this->hasOne(Courier::class, 'id', 'idCourier');
    }

    public function articulos()
    {
        return $this->hasMany(PreRegistroArticulos::class, 'idPreRegistro', 'id');
    }
}
