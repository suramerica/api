<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Empresa extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre',
        'representante',
        'correo',
        'telefono',
        'moneda',
        'monto_mayor'
    ];

    protected $casts = [
        'status' => 'boolean',
    ];

    public function usuarios()
    {
        return $this->hasMany(User::class, 'idEmpresa', 'id');
    }

    public function sucursales()
    {
        return $this->hasMany(Sucursal::class, 'idEmpresa', 'id');
    }

    public function guias()
    {
        return $this->hasMany(Guia::class, 'idEmpresa', 'id');
    }

    public function scopeWithSumPeso($query)
    {
        return $query->withCount([
            'relationship as column_sum' => function ($query) {
                $query->select(DB::raw('sum(peso)'));
            }
        ]);
    }

}
