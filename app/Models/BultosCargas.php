<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BultosCargas extends Model
{
    use HasFactory;

    protected $fillable = [
        'idCarga',
        'precinto',
        'alto',
        'ancho',
        'largo',
        'peso',
        'volumen',
    ];

    public function manifiestos ()
    {
        return $this->hasMany(ManifiestosBulto::class, 'idBulto', 'id');
    }
}
