<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ManifiestosBulto extends Model
{
    use HasFactory;

    protected $fillable = [
        'idBulto',
        'idManifiesto',
    ];

    public function manifiesto()
    {
        return $this->hasOne(Manifiesto::class, 'id', 'idManifiesto');
    }
}
