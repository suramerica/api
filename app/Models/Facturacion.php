<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Facturacion extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function empresa()
    {
        return $this->hasOne(Empresa::class, 'id', 'empresaId');
    }

    public function guias()
    {
        return $this->hasMany(FacturacionGuias::class, 'facturacionId', 'id');
    }
}
