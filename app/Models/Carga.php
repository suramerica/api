<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Carga extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idUsuario',
        'tipoCarga',
        'fechaSalida',
        'estado',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'fechaSalida' => 'date',
    ];

    public function bultos()
    {
        return $this->hasMany(BultosCargas::class, 'idCarga', 'id');
    }

    public function guias()
    {
        return $this->hasMany(Guia::class, 'idCarga', 'id');
    }

    public function manifiestos()
    {
        return $this->hasMany(Manifiesto::class, 'idCarga', 'id');
    }
}
