<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idEmpresa',
        'tipoDocumento',
        'documento',
        'nombre',
        'apellido',
        'telefono',
        'correo',
        'estado',
        'ciudad',
        'nacionalidad',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
    ];

    public function destinatarios()
    {
        return $this->hasMany(Destinatario::class, 'idCliente', 'id');
    }

    public function guias()
    {
        return $this->hasMany(Guia::class, 'idCliente', 'id');
    }
}
