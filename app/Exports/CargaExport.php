<?php

namespace App\Exports;

use App\Models\Guia;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\AfterSheet;

class CargaExport implements FromView
{
    public $carga;
    public function __construct(int $carga)
    {
        $this->carga = $carga;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $guias = Guia::where('idCarga', $this->carga)->with(['remitente', 'destinatario', 'articulos', 'sucursal'])->get();
        return view('excels.carga', compact('guias'));
    }

    public function registerEvents(): array
{
    return [
        AfterSheet::class => function (AfterSheet $event) {
            $event->sheet->prependRow(1, []); // Agrega una fila vacía en la parte superior
            $event->sheet->getStyle('A1')->applyFromArray(['alignment' => ['vertical' => 'center']]);
        },
    ];
}
}
