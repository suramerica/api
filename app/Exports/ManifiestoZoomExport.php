<?php

namespace App\Exports;

use App\Models\Guia;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ManifiestoZoomExport implements FromView
{
    public $manifiesto;
    public $tipo;
    public function __construct(int $manifiesto, $tipo)
    {
        $this->manifiesto = $manifiesto;
        $this->tipo = $tipo;
    }

    public function view(): View
    {
        $guias = Guia::where('idManifiesto', $this->manifiesto)->with(['remitente', 'destinatario', 'articulos', 'sucursal'])->get();
        if ($this->tipo == "normal" or $this->tipo == null) {
            $guias = Guia::where('idManifiesto', $this->manifiesto)->with(['remitente', 'destinatario', 'articulos', 'sucursal'])->get();
        }
        if ($this->tipo === "mayor3") {
            $guias = Guia::where('idManifiesto', $this->manifiesto)->where('peso', '>', 3)->with(['remitente', 'destinatario', 'articulos', 'sucursal'])->get();
        }
        if ($this->tipo === "menor3") {
            $guias = Guia::where('idManifiesto', $this->manifiesto)->where('peso', '<', 3)->with(['remitente', 'destinatario', 'articulos', 'sucursal'])->get();
        }
        return view('excels.manifiestozoom', compact('guias'));
    }
}
