<?php

namespace App\Exports;

use App\Models\Guia;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ManifiestoExport implements FromView
{
    public $manifiesto;
    public function __construct(int $manifiesto)
    {
        $this->manifiesto = $manifiesto;
    }

    public function view(): View
    {
        $guias = Guia::where('idManifiesto', $this->manifiesto)->with(['remitente', 'destinatario', 'articulos', 'sucursal'])->get();
        return view('excels.manifiesto', compact('guias'));
    }
}
