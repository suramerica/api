<?php

namespace App\Exports;

use App\Models\Guia;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class CargaZoomExport implements FromView
{
    public $carga;
    public $tipo;
    public function __construct(int $carga, $tipo)
    {
        $this->carga = $carga;
        $this->tipo = $tipo;
    }


    public function view(): View
    {
        $guias = Guia::where('idCarga', $this->carga)->with(['remitente', 'destinatario', 'articulos', 'sucursal'])->get();
        if ($this->tipo == "normal" or $this->tipo == null) {
            $guias = Guia::where('idCarga', $this->carga)->with(['remitente', 'destinatario', 'articulos', 'sucursal'])->get();
        }
        if ($this->tipo === "mayor3") {
            $guias = Guia::where('idCarga', $this->carga)->where('peso', '>', 3)->with(['remitente', 'destinatario', 'articulos', 'sucursal'])->get();
        }
        if ($this->tipo === "menor3") {
            $guias = Guia::where('idCarga', $this->carga)->where('peso', '<', 3)->with(['remitente', 'destinatario', 'articulos', 'sucursal'])->get();
        }
        return view('excels.manifiestozoom', compact('guias'));
    }
}
