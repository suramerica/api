<?php

namespace App\Http\Controllers;

use App\Models\Sucursal;
use App\Traits\JsonResponseTrait;
use Illuminate\Http\Request;

class SucursalController extends Controller
{
    use JsonResponseTrait;

    public function getByEmpresa($empresa)
    {
        return $this->jsonResponse(true, 'Sucursales Obtenidas con exito', Sucursal::where('idEmpresa', $empresa)->where('status', 1)->get(), 200);
    }

    public function getByEmpresaWeb($empresa)
    {
        return $this->jsonResponse(true, 'Sucursales Obtenidas con exito', Sucursal::where('idEmpresa', $empresa)->get(), 200);
    }

    public function store(Request $request)
    {
        $sucursal = Sucursal::create($request->input());

        return $this->jsonResponse(true, 'Sucursal Creada con exito', $sucursal, 200);
    }

    public function show(Sucursal $sucursale)
    {
        return $this->jsonResponse(true, 'Sucursal obtenida con exito', $sucursale, 200);
    }

    public function update(Request $request, Sucursal $sucursale)
    {
        $sucursale->nombre = $request->nombre;
        $sucursale->codigo = $request->codigo;
        $sucursale->direccion = $request->direccion;
        $sucursale->telefono = $request->telefono;
        $sucursale->save();

        return $this->jsonResponse(true, 'Sucursal actualizada con exito', $sucursale, 200);
    }

    public function destroy(Sucursal $sucursale)
    {
        if ($sucursale->status == 1) {
            $sucursale->status = 0;
        } else {
            $sucursale->status = 1;
        }
        $sucursale->save();

        return $this->jsonResponse(true, 'Sucursal actualizada con exito', $sucursale, 200);
    }
}
