<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Guia, Empresa, Facturacion, FacturacionGuias};
use App\Traits\JsonResponseTrait;
use Carbon\Carbon;

class FacturacionController extends Controller
{
    use JsonResponseTrait;

    public function index(Request $q){
        $facturas = Facturacion::query();
        $facturas->with(["empresa"]);
        if ($q->user()->typeUser == 2) {
            $facturas->where("empresaId", $q->user()->idEmpresa);
        }

        $facturas->orderBy("created_at", "DESC");
        $result = $facturas->get();

        return $this->jsonResponse(true, "Facturas Obtenidas con exito", $result, 200);
    }

    public function store(Request $q) {
        $empresas = Empresa::whereHas('guias', function ($query) use ($q) {
            $query->where('idCarga', $q->cargaId);
        })->get();

        $empresas->load(['guias' => function ($query) use ($q) {
            $query->where('idCarga', $q->cargaId);
        }]);

        // crear factura a la empresa
        foreach ($empresas as $empresa) {
            $factura = Facturacion::create([
                'cargaId' => $q->cargaId,
                'empresaId' => $empresa->id,
                'status' => false,
                'fecha_emision' => Carbon::now(),
                'fecha_vencimiento' => Carbon::now()->addDays(3)
            ]);

            $peso_facturado = 0;
            $volumen_facturado = 0;
            $monto_total = 0;

            foreach ($empresa->guias as $guia) {
                $tipo_cobro = "MENOR A 1";
                $valor_cobrado = 0;
                $monto_cobrado = 0;
                if($guia->peso < 1 && $guia->pesoVolumetrico < 1)
                {
                    $tipo_cobro = 'MENOR A 1';
                    $valor_cobrado = 1;
                    $monto_cobrado = 1 * $empresa->monto_mayor;
                    $peso_facturado = $peso_facturado + 1;
                }
                else{
                    if($guia->peso > $guia->pesoVolumetrico)
                    {
                        $tipo_cobro = "PESO";
                        $valor_cobrado = $guia->peso;
                        $monto_cobrado = $guia->peso * $empresa->monto_mayor;
                        $peso_facturado = $peso_facturado + $guia->peso;
                    }
                    if($guia->peso < $guia->pesoVolumetrico)
                    {
                        $tipo_cobro = "VOLUMEN";
                        $valor_cobrado = $guia->pesoVolumetrico;
                        $monto_cobrado = $guia->pesoVolumetrico * $empresa->monto_mayor;
                        $volumen_facturado = $volumen_facturado + $guia->pesoVolumetrico;
                    }
                }

                $monto_total = $monto_total + $monto_cobrado;
                FacturacionGuias::create([
                    'facturacionId' => $factura->id,
                    'guiaId' => $guia->id,
                    'tipo_cobro' => $tipo_cobro,
                    'valor_cobrado' => $valor_cobrado,
                    'monto_cobrado' => $monto_cobrado
                ]);
            }

            $factura->peso_facturado = $peso_facturado;
            $factura->volumen_facturado = $volumen_facturado;
            $factura->monto_total = $monto_total;
            $factura->save();
        }

        return $this->jsonResponse(true, "Facturas Generadas con exito", $empresas, 200);
    }
}
