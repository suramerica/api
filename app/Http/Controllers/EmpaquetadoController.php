<?php

namespace App\Http\Controllers;

use App\Models\Empaquetado;
use App\Traits\JsonResponseTrait;
use Illuminate\Http\Request;

class EmpaquetadoController extends Controller
{
    use JsonResponseTrait;
    public function index(Request $q)
    {
        $paquetes = Empaquetado::where('idEmpresa', $q->user()->idEmpresa)->get();

        return $this->jsonResponse(true, "Paquetes obtenidos con exito", $paquetes, 200);
    }

    public function getByEmpresa($empresa)
    {
        $paquetes = Empaquetado::where('idEmpresa', $empresa)->get();

        return $this->jsonResponse(true, "Paquetes obtenidos con exito", $paquetes, 200);
    }

    public function store(Request $q)
    {
        $paquete = Empaquetado::create($q->all());

        return $this->jsonResponse(true, "Paquete creado con exito", $paquete, 200);
    }


    public function show(Empaquetado $empaquetado)
    {
        return $this->jsonResponse(true, "Paquete Obtenido con exito", $empaquetado, 200);
    }

    public function update(Empaquetado $empaquetado, Request $q)
    {
        $empaquetado->paquete = $q->paquete;
        $empaquetado->precio = $q->precio;
        $empaquetado->divisa = $q->divisa;
        $empaquetado->save();

        return $this->jsonResponse(true, "Paquete actualizado con exito", $empaquetado, 200);
    }
}
