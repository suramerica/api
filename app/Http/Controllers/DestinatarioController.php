<?php

namespace App\Http\Controllers;

use App\Models\Destinatario;
use App\Models\Pais;
use App\Traits\JsonResponseTrait;
use Illuminate\Http\Request;

class DestinatarioController extends Controller
{
    use JsonResponseTrait;
    public function getDestinatario($documento)
    {
        $cliente = Destinatario::where('documento', $documento)->first();
        if ($cliente == null) {
            return $this->jsonResponse(false, 'Destinatario no existe', null, 200);
        } else {
            return $this->jsonResponse(true, 'Destinatario obtenido con exito', $cliente, 200);
        }
    }


    public function store(Request $request)
    {
        $pais = Pais::find($request->pais);
        $cliente = Destinatario::create($request->input());
        $cliente->nacionalidad = $pais->nacionalidad;
        $cliente->save();

        return $this->jsonResponse(true, 'Destinatario Creado con exito', $cliente, 200);
    }

    public function getByCliente($cliente)
    {
        return $this->jsonResponse(true, 'Listado de destinatario', Destinatario::where('idCliente', $cliente)->get(), 200);
    }

    public function index(Request $q)
    {
        $clientes = Destinatario::query();
        if ($q->user()->typeUser == 2) {
            $clientes->where('idEmpresa', $q->user()->idEmpresa);
        }
        if ($q->user()->typeUser == 3) {
            $clientes->where('idEmpresa', $q->user()->idEmpresa);
        }

        return $this->jsonResponse(true, 'Listado de destinatarios', $clientes->get(), 200);
    }

    public function update(Destinatario $cliente, Request $request)
    {
        $pais = Pais::find($request->pais);
        $cliente->nacionalidad = $pais->nacionalidad;
        $cliente->nombre = $request->nombre;
        $cliente->apellido = $request->apellido;
        $cliente->tipoDocumento = $request->tipoDocumento;
        $cliente->documento = $request->documento;
        $cliente->telefono = $request->telefono;
        $cliente->estado = $request->estado;
        $cliente->ciudad = $request->ciudad;
        $cliente->pais = $request->pais;
        $cliente->direccion = $request->direccion;
        $cliente->save();

        return $this->jsonResponse(true, 'Actualizado con exito', $cliente, 200);
    }
}
