<?php

namespace App\Http\Controllers;

use App\Mail\MailCredenciales;
use App\Mail\MailWelcome;
use App\Models\Empresa;
use App\Models\User;
use App\Traits\JsonResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;

class EmpresaController extends Controller
{
    use JsonResponseTrait;
    public function index()
    {
        return $this->jsonResponse(true, 'Empresas obtenidas con exito', Empresa::withCount('usuarios', 'sucursales')->get(), 200);
    }

    public function store(Request $request)
    {
        $empresa = Empresa::create($request->input());

        return $this->jsonResponse(true, 'Empresa Registrada con exito', $empresa, 200);
    }

    public function show($empresa)
    {
        return $this->jsonResponse(true, 'Empresa Obtenida con exito', Empresa::where('id', $empresa)->with('usuarios', 'sucursales')->first(), 200);
    }

    public function update(Request $request, Empresa $empresa)
    {
        $empresa->nombre = $request->nombre;
        $empresa->representante = $request->representante;
        $empresa->correo = $request->correo;
        $empresa->telefono = $request->telefono;
        $empresa->moneda = $request->moneda;
        $empresa->monto_mayor = $request->monto_mayor;
        $empresa->save();

        return $this->jsonResponse(true, 'Empresa Actualizada con exito', $empresa, 200);
    }

    // public function status(Empresa $empresa, $estado)
    // {
    //     return $estado;
    //     $empresa->status = $estado;
    //     $empresa->save();

    //     return $this->jsonResponse(true, 'Empresa Actualizada con exito', $empresa, 200);
    // }

    public function status(Request $request, Empresa $empresa, $estado)
    {
        if($empresa->status == true)
        {
            $empresa->status = false;
        }
        else{
            $empresa->status = true;
        }
        $empresa->save();
        $usuarios = User::where('idEmpresa', $empresa->id)->get();

        foreach ($usuarios as $usuario) {
            $usuario->status = $empresa->status;
            $usuario->save();
        }
        return $this->jsonResponse(true, 'Empresa Actualizada con exito', $empresa, 200);
    }

    public function tasa(Request $request)
    {
        $empresa = Empresa::find($request->id);
        $empresa->tasa = $request->tasa;
        $empresa->save();

        return $this->jsonResponse(true, 'Tasa Actualizada', $empresa, 200);
    }

    public function welcome(Empresa $empresa)
    {
        Mail::to($empresa->correo)->send(new MailWelcome($empresa));
        $usuario = User::where('idEmpresa', $empresa->id)->where('typeUser', 2)->first();
        $clave = rand();
        $usuario->password = Hash::make($clave);
        $usuario->save();
        Mail::to($empresa->correo)->send(new MailCredenciales($clave, $usuario));
        return $this->jsonResponse(true, 'Correo de bienvenida y credenciales enviado correctamente', $empresa, 200);
    }
}
