<?php

namespace App\Http\Controllers;
use App\Models\{Cliente};
use App\Models\{PreRegistro, PreRegistroArticulos};
use App\Traits\JsonResponseTrait;
use Illuminate\Http\Request;

class PreRegistroController extends Controller
{
    use JsonResponseTrait;
    public function getDocument($documento, $tipoDocumento)
    {
        $remitente = Cliente::where("documento", $documento)->where('tipoDocumento', $tipoDocumento)->first();
        if($remitente != null)
        {
            return $this->jsonResponse(true, 'Remitente encontrado con exito', $remitente, 200);
        }
        else{
            return $this->jsonResponse(false, 'Remitente no existe, Registre sus datos!', null, 200);
        }
    }

    public function store(Request $q)
    {
        $articulos = $q->articulos;
        $registro = PreRegistro::create($q->input());

        foreach($articulos as $articulo)
        {
            $datos = [
                'idPreRegistro' => $registro->id,
                'nombre' => $articulo['producto'],
                'cantidad' => $articulo['cantidad'],
                'precio' => $articulo['precio'],
            ];
            $reg = PreRegistroArticulos::create($datos);
        }

        return $this->jsonResponse(true, 'PreRegistro Con exito', $registro, 200);
    }

    public function index(Request $q)
    {
        $preRegistros = PreRegistro::query();
        $preRegistros->with(['remitente', 'destinatario', 'oficina', 'articulos'])->withCount('articulos');
        if ($q->user()->typeUser == 2) {
            $preRegistros->where('idEmpresa', $q->user()->idEmpresa);
        }
        if ($q->user()->typeUser == 3) {
            $preRegistros->where('idSucursal', $q->user()->idSucursal);
        }

        return $this->jsonResponse(true, 'PreRegistros Listado', $preRegistros->where('status', false)->orderByDesc('id')->get(), 200);
    }
}
