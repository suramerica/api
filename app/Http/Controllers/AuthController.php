<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Traits\JsonResponseTrait;
use Illuminate\Support\Facades\Http;


class AuthController extends Controller
{
    use JsonResponseTrait;
    public function create(Request $request)
    {
        $user = User::create([
            'name' => strtoupper($request->name),
            'email' => strtoupper($request->email),
            'password' => Hash::make($request->password),
            'typeUser' => $request->typeUser,
            'idEmpresa' => $request->idEmpresa,
            'idSucursal' => $request->idSucursal
        ]);


        return $this->jsonResponse(true, 'Usuario Creado con exito', $user, 200, $user->createToken('APITOKEN'));
    }

    public function update(User $usuario, Request $request)
    {
        $usuario->name = strtoupper($request->name);
        $usuario->phone = $request->phone;
        $usuario->idSucursal = $request->idSucursal;

        if ($request->password) {
            $usuario->password = Hash::make($request->password);
        }

        $usuario->save();
        return $this->jsonResponse(true, 'Usuario actualizado con exito', $usuario, 200);
    }

    public function login(Request $request)
    {
        if (!Auth::attempt($request->only('email', 'password'))) {
            return $this->jsonResponse(false, 'Datos Erroneos', null, 401, null, 'Unauthorized');
        } else {
            $user = User::where('email', $request->email)->with('empresa')->first();
            return $this->jsonResponse(true, 'Usuario Logueado con exito', $user, 200, $user->createToken('APITOKEN'));
        }
    }

    public function logout()
    {
        auth()->user()->tokens()->delete();
        return $this->jsonResponse(true, 'Sesion Cerrada con exito', null, 200);
    }

    public function terminos(Request $request, $ip)
    {
        $usuario = Auth::user();
        $usuario->ipAcept = $ip;
        $usuario->terminos = true;
        $usuario->save();

        return $this->jsonResponse(true, 'Terminos Aceptados con exito', null, 200);
    }
}
