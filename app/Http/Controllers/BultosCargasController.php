<?php

namespace App\Http\Controllers;

use App\Models\BultosCargas;
use App\Models\{Manifiesto, Guia};
use App\Models\ManifiestosBulto;
use App\Traits\JsonResponseTrait;
use App\Traits\TrackingGuiaTrait;
use Illuminate\Http\Request;

class BultosCargasController extends Controller
{
    use JsonResponseTrait;
    use TrackingGuiaTrait;
    public function store(Request $request)
    {
        $bulto = BultosCargas::create([
            'idCarga' => $request->idCarga,
            'precinto' => $request->precinto,
            'alto' => $request->alto,
            'ancho' => $request->ancho,
            'largo' => $request->largo,
            'peso' => 0,
            'volumen' => ($request->alto * $request->ancho * $request->largo) / 5000
        ]);

        // ingresa los manifiestos al bulto
        $manifiestos = $request->manifiestos;

        foreach ($manifiestos as $manifiesto) {
            if($manifiesto['status'] == true)
            {
                ManifiestosBulto::create([
                    'idBulto' => $bulto->id,
                    'idManifiesto' => $manifiesto['guia']
                ]);

                $manifiesto = Manifiesto::find($manifiesto['guia']);
                $manifiesto->idCarga = $request->idCarga;
                $manifiesto->save();
            }
        }
        $peso = $this->calcularPesoBulto($bulto->id);

        return $this->jsonResponse(true, 'Precinto creado con exito', $bulto, 200);
    }

    public function get($carga)
    {
        return $this->jsonResponse(true, 'Precinto creado con exito', Guia::where('escaneada', 1)->where('idCarga', $carga)->with('empresa')->get(), 200);
    }

    public function delete(Guia $guia)
    {
        $carga = $guia->idCarga;
        $guia->escaneada = 0;
        $guia->idCarga = null;
        $guia->save();

        return $this->jsonResponse(true, 'Precinto creado con exito', Guia::where('escaneada', 1)->where('idCarga', $carga)->with('empresa')->get(), 200);
    }
}
