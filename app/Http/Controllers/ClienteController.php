<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use App\Models\Pais;
use App\Traits\JsonResponseTrait;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    use JsonResponseTrait;
    public function getCliente($documento)
    {
        $cliente = Cliente::where("documento", $documento)
            ->with("destinatarios")
            ->first();
        if ($cliente == null) {
            return $this->jsonResponse(false, "Cliente no existe", null, 200);
        } else {
            return $this->jsonResponse(
                true,
                "Cliente obtenido con exito",
                $cliente,
                200
            );
        }
    }

    public function index(Request $q)
    {
        $clientes = Cliente::query();
        $clientes->withCount("destinatarios", "guias");
        if ($q->user()->typeUser == 2) {
            $clientes->where("idEmpresa", $q->user()->idEmpresa);
        }
        if ($q->user()->typeUser == 3) {
            $clientes->where("idEmpresa", $q->user()->idEmpresa);
        }

        return $this->jsonResponse(
            true,
            "Listado de clientes",
            $clientes->get(),
            200
        );
    }

    public function store(Request $request)
    {
        $cliente = Cliente::where("documento", $request->documento)->first();
        if ($cliente != null) {
            return $this->jsonResponse(
                false,
                "Error CLiente ya existe",
                $cliente,
                200
            );
        } else {
            $pais = Pais::find($request->pais);
            $cliente = Cliente::create($request->input());
            $cliente->nacionalidad = $pais->nacionalidad;
            $cliente->pais = $pais->id;
            $cliente->save();

            return $this->jsonResponse(
                true,
                "Cliente Creado con exito",
                $cliente,
                200
            );
        }
    }

    public function update(Cliente $cliente, Request $request)
    {
        $pais = Pais::find($request->pais);
        $cliente->nombre = $request->nombre;
        $cliente->apellido = $request->apellido;
        $cliente->tipoDocumento = $request->tipoDocumento;
        $cliente->documento = $request->documento;
        $cliente->telefono = $request->telefono;
        $cliente->correo = $request->correo;
        $cliente->estado = $request->estado;
        $cliente->ciudad = $request->ciudad;
        $cliente->pais = $request->pais;
        $cliente->nacionalidad = $pais->nacionalidad;
        $cliente->save();

        return $this->jsonResponse(
            true,
            "Actualizado con exito",
            $cliente,
            200
        );
    }
}
