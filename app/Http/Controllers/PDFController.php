<?php

namespace App\Http\Controllers;

use App\Exports\CargaExport;
use App\Exports\CargaZoomExport;
use App\Exports\ManifiestoExport;
use App\Exports\ManifiestoZoomExport;
use App\Models\Empresa;
use App\Models\Guia;
use App\Models\Manifiesto;
use App\Models\Facturacion;
use Maatwebsite\Excel\Facades\Excel;

class PDFController extends Controller
{
    public function excelManifiesto($manifiesto, $tipo)
    {
        return Excel::download(new ManifiestoExport($manifiesto), 'manifiesto-' . $manifiesto . '.xlsx');
    }

    public function excelZoomManifiesto($manifiesto, $tipo)
    {
        return Excel::download(new ManifiestoZoomExport($manifiesto, $tipo), 'manifiesto-' . $manifiesto . '.xlsx');
    }

    public function excelCarga($carga)
    {
        return Excel::download(new CargaExport($carga), 'carga-' . $carga . '.xlsx');
    }

    public function excelZoomCarga($carga, $tipo)
    {
        return Excel::download(new CargaZoomExport($carga, $tipo), 'carga-'.$tipo.'-'. $carga . '.xlsx');
    }

    // public function previewCarga($carga)
    // {
    //     $guias = Guia::where('idCarga', $carga)->with(['remitente', 'destinatario', 'articulos', 'sucursal'])->get();
    //     return view('excels.carga', compact('guias'));

    // }

    public function ticketGuia($guia)
    {
        $guia = Guia::where('id', $guia)->with('remitente', 'destinatario', 'sucursal', 'oficina', 'articulos')->first();
        $empresa = Empresa::find($guia->idEmpresa);
        // return $guia;
        $pdf = \PDF::loadView('pdfs.tickect', ['guia' => $guia, 'empresa' => $empresa]);
        $pdf->setPaper('a5', 'landscape');
        $pdf->set_paper(array(0, 0, 227, 1050));
        $pdf->render();
        return $pdf->stream("Ticket-" . $guia->sucursal->codigo . $guia->id . ".pdf");
    }

    public function guiaEtiqueta($guia)
    {
        $guia = Guia::where('id', $guia)->with('remitente', 'destinatario', 'sucursal', 'articulos')->first();
        $empresa = Empresa::find($guia->idEmpresa);
        // $codigoQR = QrCode::format('png')->size(25)->generate('texto');
        // return $guia;
        $pdf = \PDF::loadView('pdfs.etiqueta', ['guia' => $guia, 'empresa' => $empresa]);
        $pdf->setPaper('a5', 'portrait');
        $pdf->render();
        return $pdf->stream("Etiqueta-" . $guia->sucursal->codigo . $guia->id . ".pdf");
    }

    public function facturaEmpresa($factura)
    {
        $factura = Facturacion::where('id', $factura)->with(['guias', 'empresa'])->first();
        $pdf = \PDF::loadView('pdfs.factura', ['factura' => $factura]);
        $pdf->setPaper('a4', 'portrait');
        $pdf->render();
        return $pdf->stream("Factura-" . $factura->id . ".pdf");
    }

    public function guiaGuia($guia)
    {
        $guia = Guia::where('id', $guia)->with('remitente', 'destinatario', 'sucursal', 'oficina', 'articulos')->first();
        // return $guia;
        $empresa = Empresa::find($guia->idEmpresa);
        $pdf = \PDF::loadView('pdfs.guia', ['guia' => $guia, 'empresa' => $empresa]);
        $pdf->render();
        return $pdf->stream("Guia-" . $guia->sucursal->codigo . $guia->id . ".pdf");
    }

    public function parkingGuia($guia)
    {
        $guia = Guia::where('id', $guia)->with('remitente', 'destinatario', 'sucursal', 'articulos')->first();
        $empresa = Empresa::find($guia->idEmpresa);
        // return $guia;
        $pdf = \PDF::loadView('pdfs.parking', ['guia' => $guia, 'empresa' => $empresa]);
        $pdf->render();
        return $pdf->stream("ParkinLit-" . $guia->sucursal->codigo . $guia->id . ".pdf");
    }

    public function manifiestoPdf($manifiesto)
    {
        $manifiesto = Manifiesto::where('id', $manifiesto)->with(['creador', 'empresa', 'sucursal', 'guias', 'guias.destinatario'])->first();
        $peso = Guia::where('idManifiesto', $manifiesto->id)->get()->sum('peso');
        $pdf = \PDF::loadView('pdfs.manifiesto', ['manifiesto' => $manifiesto, 'peso' => $peso]);
        $pdf->setPaper('a4', 'landscape');
        $pdf->render();
        return $pdf->stream("Manifiesto-" . $manifiesto->sucursal->codigo . $manifiesto->id . ".pdf");
    }
}
