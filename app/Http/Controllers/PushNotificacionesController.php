<?php

namespace App\Http\Controllers;

use App\Models\PushNotificaciones;
use App\Traits\JsonResponseTrait;
use App\Traits\PushTrait;
use Illuminate\Http\Request;

class PushNotificacionesController extends Controller
{
    use JsonResponseTrait;
    use PushTrait;

    public function store(Request $q)
    {
        $message = "Bienvenido al sistema de seguimiento por notificaciones de Tracking de Sur America Cargo, has consultado la guia numero: " . $q->idGuia . ' a partir de ahora recibiras una actualizacion cada vez que esta cambie de estado';
        $this->sendPush($q->token, $message);
        $validate = PushNotificaciones::where('idGuia', $q->idGuia)->where('token', $q->token)->count();
        if ($validate > 0) {
            $push = PushNotificaciones::where('idGuia', $q->idGuia)->where('token', $q->token)->first();
        } else {
            $push = PushNotificaciones::create($q->input());
        }
        return $this->jsonResponse(true, 'Notificaciones Registradas con exito', null, 200);
    }
}
