<?php

namespace App\Http\Controllers;

use App\Models\Tasa;
use App\Traits\JsonResponseTrait;
use Illuminate\Http\Request;

class TasaController extends Controller
{
    use JsonResponseTrait;
    public function store(Request $request)
    {
        $tasa = Tasa::create($request->input());

        return $this->jsonResponse(true, 'Tasa creada con exito', $tasa, 200);
    }

    public function getByEmpresa($empresa)
    {
        return $this->jsonResponse(true, 'Tasas obtenidas con exito', Tasa::where('idEmpresa', $empresa)->get(), 200);
    }

    public function getByEmpresaWeb($empresa)
    {
        return $this->jsonResponse(true, 'Tasas obtenidas con exito', Tasa::where('idEmpresa', $empresa)->get(), 200);
    }

    public function getByGuia($empresa, $tipo)
    {
        return $this->jsonResponse(true, 'Tasas obtenidas con exito', Tasa::where('idEmpresa', $empresa)->where('tipoEnvio', $tipo)->get(), 200);
    }

    public function getByCalculadora($empresa)
    {
        return $this->jsonResponse(true, 'Tasas obtenidas con exito', Tasa::where('idEmpresa', $empresa)->where('tipoPrecio', 'CALCULADO')->get(), 200);
    }

    public function show(Tasa $tasa)
    {
        return $this->jsonResponse(true, 'Tasa Obtenida con exito', $tasa, 200);
    }

    public function update(Request $request, Tasa $tasa)
    {
        $tasa->tasa = $request->tasa;
        $tasa->tipoPrecio = $request->tipoPrecio;
        $tasa->tipoEnvio = $request->tipoEnvio;
        $tasa->monto = $request->monto;
        $tasa->divisa = $request->divisa;
        $tasa->save();

        return $this->jsonResponse(true, 'Tasa Actualizada con exito', $tasa, 200);
    }
}
