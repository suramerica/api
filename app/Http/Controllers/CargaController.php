<?php

namespace App\Http\Controllers;

use App\Models\Carga;
use App\Models\CargaTracking;
use App\Models\Guia;
use App\Models\Manifiesto;
use App\Traits\JsonResponseTrait;
use App\Traits\TrackingCargaTrait;
use App\Traits\TrackingGuiaTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class CargaController extends Controller
{
    use JsonResponseTrait;
    use TrackingGuiaTrait;
    use TrackingCargaTrait;
    public function index(Request $q)
    {
        $cargas = Carga::query();
        $cargas->withCount('guias', 'manifiestos');
        if ($q->user()->typeUser == 2) {
            $cargas->where('idEmpresa', $q->user()->idEmpresa);
        }
        if ($q->user()->typeUser == 3) {
            $cargas->where('idUsuario', $q->user()->id);
        }

        return $this->jsonResponse(true, 'Listado de Cargas', $cargas->orderByDesc('id')->get(), 200);
    }

    public function store(Request $request)
    {
        $carga = Carga::create([
            'idUsuario' => $request->user()->id,
            'tipoCarga' => $request->tipoCarga,
            'fechaSalida' => $request->fecha,
            'estado' => 'CREACION',
        ]);

        return $this->jsonResponse(true, 'Carga creada con exito', $carga, 200);
    }

    public function cerrar($carga)
    {
        $carga = Carga::find($carga);

        $guias = Guia::where('idCarga', $carga->id)->get();
        foreach ($guias as $guia) {
            $this->asignarEstado($guia->id, 'Aduana origen', null, null);
            $guia->save();
        }

        $carga->estado = 'Aduana origen';
        $carga->save();

        $this->asignarTrackingCarga($carga->id, "Aduana origen");

        return $this->jsonResponse(true, 'Carga Cerrada con exito', $carga, 200);
    }

    public function trackingEstado($carga, $estado)
    {
        $this->asignarTrackingCarga($carga, $estado);
        $guias = Guia::where('idCarga', $carga)->get();
        foreach ($guias as $guia) {
            $this->asignarEstado($guia->id, $estado);
        }

        return $this->jsonResponse(true, 'Tracking Asignado con exito a carga y guias', $guias, 200);
    }

    public function getTrackings($carga)
    {
        $trackings = CargaTracking::where('idCarga', $carga)->get();

        return $this->jsonResponse(true, 'Trackings obtenidos con exito', $trackings, 200);
    }

    public function setTracking(Carga $carga, $estado)
    {
        $carga->estado = $estado;
        $carga->save();

        $guias = Guia::where('idCarga', $carga->id)->get();
        foreach ($guias as $guia) {
            $guia->idCarga = $carga->id;
            $this->asignarEstado($guia->id, $estado, null, null);
            $guia->save();
        }
    }

    public function CronTrackingCarga()
    {
        // Procesos de Carga
        $cargas = Carga::all();

        foreach ($cargas as $carga) {
            if ($carga->estado == 'Aduana origen') {
                $inicio = new \DateTime($carga->created_at);
                $fin = new \DateTime(now());
                $intervalo = $inicio->diff($fin);
                if ($intervalo->d > 0) {
                    $carga->estado = 'En Transito';
                    $carga->save();
                    $this->asignarTrackingCarga($carga->id, 'En Transito');
                    $guias = Guia::where('idCarga', $carga->id)->get();
                    foreach ($guias as $guia) {
                        $this->asignarEstado($guia->id, 'En Transito');
                    }
                }
            }

            if ($carga->estado == 'En Transito') {
                $inicio = new \DateTime($carga->created_at);
                $fin = new \DateTime(now());
                $intervalo = $inicio->diff($fin);
                if ($intervalo->d > 2) {
                    $carga->estado = 'Aduana destino';
                    $carga->save();
                    $this->asignarTrackingCarga($carga->id, 'Aduana destino');
                    $guias = Guia::where('idCarga', $carga->id)->get();
                    foreach ($guias as $guia) {
                        $this->asignarEstado($guia->id, 'Aduana destino');
                    }
                }
            }
        }

        // procesos de zoom
        $fin = date('d-m-Y');
        $inicio = date("d-m-Y", strtotime($fin . "- 7 days"));

        $url = "https://serviciosbd.zoom.red/guiaelectronica/guiasClientes?codcliente=450104&fechaDesde=$inicio&fechaHasta=$fin";

        $response = Http::withoutVerifying()->get($url);
        $guias = $response['entidadRespuesta'];

        foreach ($guias as $guia) {
            if (count(explode('-', $guia['referencia'])) > 2) {
                $explode = explode('-', $guia['referencia']);
                $slice = array_slice($explode, -2, 1);
                $guiaId = end($slice);
            }
            if (count(explode('-', $guia['referencia'])) == 2) {
                $explode = explode('-', $guia['referencia']);
                $guiaId = $explode[1];
            }

            if(isset($guiaId))
            {
                $guiaD = Guia::find($guiaId);
                if ($guiaD != null) {
                    $guiaD->guiaZoom = $guia['codguia'];
                    $guiaD->save();
                }
            }

        }

        return $this->jsonResponse(true, 'Actualizacion de tracking y zoom correcta', null, 200);
    }

    public function detalle($carga)
    {
        return $this->jsonResponse(true, 'Obtenidos con exito', [
            'guias' => Guia::where('idCarga', $carga)->with('sucursal')->get(),
            'manifiestos' => Manifiesto::where('idCarga', $carga)->with('sucursal')->get(),
        ], 200);
    }

    public function addGuia(Request $request)
    {
        $carga = $request->idCarga;

        $guia = Guia::find($request->idGuia);
        $guia->idCarga = $carga;
        $guia->escaneada = 1;
        $guia->save();

        return $this->jsonResponse(true, 'Guia Agregada Correctamente', $guia, 200);
    }

    public function eliminar(Carga $carga)
    {
        $manifiestos = Manifiesto::where('idCarga', $carga->id)->get();

        foreach ($manifiestos as $manifiesto) {
            $manifiesto->idCarga = null;
            $manifiesto->save();
            $guias = Guia::where('idManifiesto', $manifiesto->id)->get();
            foreach ($guias as $guia) {
                $guia->idCarga = null;
                $guia->save();
            }
        }
        $carga->delete();
    }
}
