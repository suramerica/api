<?php

namespace App\Http\Controllers;

use App\Models\Courier;
use App\Traits\JsonResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class CourierController extends Controller
{
    use JsonResponseTrait;
    public function generate()
    {
        // Limpiamos la tabla
        Courier::truncate();
        // Obtener Estado
        $result = Http::get('https://api.zoom.red/canguroazul/getEstados?filtro=3');
        $estados = $result['entidadRespuesta'];
        // recorre los estados
        foreach ($estados as $estado) {
            $result = Http::get('https://api.zoom.red/canguroazul/getOficinaEstadoWs?codestado=' . $estado['codestado']);
            $oficinas = $result['entidadRespuesta'];

            foreach ($oficinas as $oficina) {
                // return $oficina;
                // registra cada oficina
                Courier::create([
                    'nombre' => $oficina['nombre'],
                    'courier' => 'ZOOM',
                    'CodOficina' => $oficina['codoficina'],
                    'estado' => $oficina['nombre_estado'],
                    'ciudad' => $oficina['nombre_ciudad'],
                    'direccion' => $oficina['direccion'],
                    'codEstado' => $oficina['codestado'],
                    'codCiudad' => $oficina['codciudad']
                ]);
            }
        }
    }

    public function store(Request $request)
    {
        $courier = Courier::create($request->input());

        return $this->jsonResponse(true, 'Oficina Courier Creada con exito', $courier, 200);
    }

    public function index()
    {
        $estados = Courier::select('estado')->distinct()->get();
        $ciudades = Courier::select('ciudad', DB::raw('MAX(estado) as estado'))->groupBy('ciudad')->get();
        $courier = Courier::orderBy('estado', 'ASC')->get();

        $data = [
            'estados' => $estados,
            'ciudades' =>$ciudades,
            'oficinas' => $courier
        ];

        return $this->jsonResponse(true, 'Listado de Oficinas', $data, 200);
    }

    public function filterByAddres($address, $estado)
    {
        $dir = strtoupper($address);
        $courier = Courier::where('direccion', 'like', "%$dir%")->where('estado', $estado)->get();

        return $this->jsonResponse(true, 'Listado de Oficinas Filtrado', $courier, 200);
    }

}
