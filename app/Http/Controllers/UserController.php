<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Traits\JsonResponseTrait;
use Illuminate\Http\Request;

class UserController extends Controller
{
    use JsonResponseTrait;
    public function show(User $usuario)
    {
        return $this->jsonResponse(true, 'Usuario obtenido con extio', $usuario, 200);
    }

    public function getByEmpresa($empresa)
    {
        return $this->jsonResponse(true, 'Usuarios obtenidos con extio', User::where('idEmpresa', $empresa)->get(), 200);
    }

    public function status(User $usuario, $estado)
    {
        $usuario->status = $estado;
        $usuario->save();

        return $this->jsonResponse(true, 'Usuario Actualizaco con exito', $usuario, 200);
    }
}
