<?php

namespace App\Http\Controllers;

use App\Models\Pais;
use App\Traits\JsonResponseTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class PaisController extends Controller
{
    use JsonResponseTrait;
    public function getAll()
    {
        $response = Http::get('https://restcountries.com/v3.1/all');
        for ($i=0; $i < 300; $i++) { 
            Pais::create([
                'bandera' => $response[$i]['flag'],
                'nombre' => $response[$i]['name']['common']
            ]);
        }
    }

    public function index()
    {
        return $this->jsonResponse(true, 'Paises Obtenidos con exito', Pais::orderBy('status', 'DESC')->get(), 200);
    }

    public function activos()
    {
        return $this->jsonResponse(true, 'Paises Exito', Pais::where('status', 1)->get(), 200);
    }

    public function actives()
    {
        return $this->jsonResponse(true, 'Paises Activos Obtenidos con exito', Pais::where('status', 1)->get(), 200);
    }

    public function update(Request $request, Pais $paise)
    {
        $paise->status = $request->status;
        $paise->save();

        return $this->jsonResponse(true, 'Pais Actualizado con exito', $paise, 200);
    }
}
