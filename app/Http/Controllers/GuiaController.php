<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use App\Models\Empresa;
use App\Models\Guia;
use App\Models\Manifiesto;
use App\Models\GuiaArticulo;
use App\Models\GuiaPago;
use App\Models\Pais;
use App\Models\PreRegistro;
use App\Models\Sucursal;
use App\Models\User;
use App\Traits\GuiaZoomTrait;
use App\Traits\JsonResponseTrait;
use App\Traits\TrackingGuiaTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Shuchkin\SimpleXLSX;
use Carbon\Carbon;

class GuiaController extends Controller
{
    use JsonResponseTrait;
    use TrackingGuiaTrait;
    use GuiaZoomTrait;
    public function generate(Request $request)
    {
        $paisOrigen = Pais::find($request->paisOrigen);
        $paisDestino = Pais::find($request->paisDestino);
        $usuario = User::find($request->idUsuario);
        $sucursal = Sucursal::find($usuario->idSucursal);

        $guia = Guia::create([
            "idUsuario" => $request->idUsuario,
            "idEmpresa" => $request->idEmpresa,
            "idSucursal" => $usuario->idSucursal,
            "idCliente" => $request->idCliente,
            "idDestinatario" => $request->idDestinatario,
            "idTasa" => $request->idTasa,
            "tipoGuia" => $request->tipoGuia,
            "alto" => $request->alto,
            "ancho" => $request->ancho,
            "largo" => $request->largo,
            "pesoVolumetrico" =>
                ($request->alto * $request->ancho * $request->largo) / 5000,
            "m3" =>
                ($request->alto * $request->ancho * $request->largo) / 1000000,
            "ft3" =>
                $request->alto *
                $request->ancho *
                $request->largo *
                0.0000353147,
            "peso" => $request->peso,
            "divisa" => $request->divisa,
            "paisOrigen" => $paisOrigen->nombre,
            "paisDestino" => $paisDestino->nombre,
            "monto" => $request->monto,
            "seguro" => $request->seguro,
            "montoSeguro" => $request->montoSeguro,
            "delivery" => $request->delivery,
            "montoDelivery" => $request->montoDelivery,
            "paquete" => $request->paquete,
            "paqueteNombre" => $request->paqueteNombre,
            "paquetePrecio" => $request->paquetePrecio,
        ]);

        if ($request->puertaPuerta) {
            $guia->entregaPP = true;
            $guia->idOficinaCourier = $request->CodOficina;
            $guia->save();
        }

        // Preregistro
        if ($request->preRegistro == true) {
            $pre = PreRegistro::find($request->idPreRegistro);
            $pre->status = 1;
            $pre->save();
        }

        $this->asignarEstado($guia->id, "Creacion", $sucursal->nombre, null);
        // return $this->generateShipment($guia->id, $guia->tipoGuia);
        return $this->jsonResponse(true, "Guia Generada con exito", $guia, 200);
    }

    public function generateIntegracion(Request $request)
    {
        $sucursal = Sucursal::find(24);
        $guia = Guia::create([
            "idUsuario" => auth()->user()->id,
            "idEmpresa" => 11,
            "idSucursal" => 24,
            "idCliente" => $request->idCliente,
            "idDestinatario" => $request->idDestinatario,
            "idTasa" => $request->idTasa,
            "tipoGuia" => $request->tipoGuia,
            "alto" => $request->alto,
            "ancho" => $request->ancho,
            "largo" => $request->largo,
            "pesoVolumetrico" =>
                ($request->alto * $request->ancho * $request->largo) / 5000,
            "m3" =>
                ($request->alto * $request->ancho * $request->largo) / 1000000,
            "ft3" =>
                $request->alto *
                $request->ancho *
                $request->largo *
                0.0000353147,
            "peso" => $request->peso,
            "divisa" => $request->divisa,
            "paisOrigen" => $request->paisOrigen,
            "paisDestino" => $request->paisDestino,
            "monto" => $request->monto,
            "seguro" => $request->seguro,
            "montoSeguro" => $request->montoSeguro,
            "delivery" => $request->delivery,
            "montoDelivery" => $request->montoDelivery,
            "paquete" => $request->paquete,
            "paqueteNombre" => $request->paqueteNombre,
            "paquetePrecio" => $request->paquetePrecio,
        ]);

        if ($request->entregaPP) {
            $guia->entregaPP = true;
            $guia->idOficinaCourier = $request->idOficinaCourier;
            $guia->save();
        }

        $this->asignarEstado($guia->id, "Creacion", $sucursal->nombre, null);
        return $this->jsonResponse(true, "Guia Generada con exito", $guia, 200);
    }

    public function articulos(Request $request)
    {
        $guia = Guia::find($request->guia);

        foreach ($request->articulos as $articulo) {
            GuiaArticulo::create([
                "idGuia" => $guia->id,
                "cantidad" => $articulo["cantidad"],
                "monto" => $articulo["precio"],
                "producto" => $articulo["nombre"],
                "bateria" => 0,
            ]);
        }

        foreach ($request->pagos as $pago) {
            GuiaPago::create([
                "idGuia" => $guia->id,
                "tipoPago" => $pago["tipoPago"],
                "divisa" => $guia["divisa"],
                "monto" => $pago["monto"],
            ]);
        }

        return $this->jsonResponse(true, "Adicionales Creados", null, 200);
    }

    public function listado(Request $q)
    {
        $guias = Guia::query();
        $guias->with(["destinatario", "sucursal", "remitente", "oficina"]);
        if ($q->rangoFecha) {
        } else {
            $guias->whereDate("created_at", date("Y-m-d"));
        }

        if ($q->user()->typeUser == 2) {
            $guias->where("idEmpresa", $q->user()->idEmpresa);
        }
        if ($q->user()->typeUser == 3) {
            $guias->where("idUsuario", $q->user()->id);
        }

        if ($q->fecha) {
            $guias->where("created_at", $q->fecha);
        }
        if ($q->tipo) {
            $guias->where("tipoGuia", $q->tipo);
        }
        if ($q->origen) {
            $guias->where("paisOrigen", $q->origen);
        }
        if ($q->destino) {
            $guias->where("paisDestino", $q->destino);
        }
        $guias->orderBy("created_at", "DESC");
        $result = $guias->paginate("500");
        return $this->jsonResponse(
            true,
            "Guias Obtenidas con exito",
            $result,
            200
        );
    }

    public function filtrada(Request $q)
    {
        $guias = Guia::query();
        $guias->with("sucursal", "destinatario");
        if ($q->user()->typeUser == 2) {
            $guias->where("idEmpresa", $q->user()->idEmpresa);
        }
        if ($q->user()->typeUser == 3) {
            $guias->where("idUsuario", $q->user()->id);
        }
        // Si no trae ningun filtro fultra por la fecha de hoy
        if (
            !$q->guia &&
            !$q->sucursal &&
            !$q->cliente &&
            !$q->fechaInicial &&
            !$q->fechaFintal
        ) {
            $guias->whereDate("created_at", date("Y-m-d"));
        }
        // si no trae un idGuia filtra de forma anidada
        if (!$q->guia) {
            // suma sucursal
            if ($q->sucursal) {
                $guias->where("idSucursal", $q->sucursal);
            }
            // suma por cliente
            if ($q->cliente) {
                $guias->where("idCliente", $q->cliente);
            }
            // entra por fechas
            if ($q->fechaInicial) {
                $inicial = $q->fechaInicial[0] / 1000;
                $final = $q->fechaInicial[1] / 1000;
                $ini = Carbon::createFromTimestamp($inicial);
                $fin = Carbon::createFromTimestamp($final);
                $guias->whereBetween("created_at", [
                    explode("T", $ini)[0],
                    explode("T", $fin)[0],
                ]);
            }
        } else {
            $guias->where("id", $q->guia);
        }

        $guias->orderBy("created_at", "DESC");
        $result = $guias->paginate("1000");
        return $this->jsonResponse(
            true,
            "Guias Filtradas con exito",
            $result,
            200
        );
    }

    public function filtrosfechas(Request $q)
    {
        $guias = Guia::query();
        $guias->with("sucursal", "destinatario", "remitente");
        if ($q->user()->typeUser == 2) {
            $guias->where("idEmpresa", $q->user()->idEmpresa);
        }
        if ($q->user()->typeUser == 3) {
            $guias->where("idUsuario", $q->user()->id);
        }

        // Si no trae ningun filtro fultra por la fecha de hoy
        if ( !$q->fechas ) {
            $guias->whereDate("created_at", date("Y-m-d"));
        }
        else{
            $guias->whereBetween("created_at", [
                explode("T", $q->fechas[0])[0],
                explode("T", $q->fechas[1])[0],
            ]);
        }

        $guias->orderBy("created_at", "DESC");
        $result = $guias->paginate("1000");
        return $this->jsonResponse(
            true,
            "Guias Filtradas con exito",
            $result,
            200
        );
    }

    public function estadisticasHomeNew(Request $q)
    {
        $hoy = Carbon::today();

        // operario
        if ($q->user()->typeUser == 3) {
            $guias = Guia::where("idUsuario", $q->user()->id)->get();
            $guiasDay = Guia::where('idUsuario', $q->user()->id)
                  ->whereDate('created_at', $hoy)
                  ->get();
            $manifiestos = Manifiesto::where("idUsuario", $q->user()->id)->count();
        }

        // empresa
        if ($q->user()->typeUser == 2) {
            $guias = Guia::where("idEmpresa", $q->user()->idEmpresa)->get();
            $guiasDay = Guia::where('idEmpresa',  $q->user()->idEmpresa)
                  ->whereDate('created_at', $hoy)
                  ->get();
            $manifiestos = Manifiesto::where("idEmpresa",  $q->user()->idEmpresa)->count();
        }

        if ($q->user()->typeUser == 1) {
            $guias = Guia::all();
            $guiasDay = Guia::whereDate('created_at', $hoy)
                  ->get();
            $manifiestos = Manifiesto::count();
        }

        $peso = 0;
        foreach ($guias as $guia) {
            $peso += $guia->peso;
        }

        $pesoHoy = 0;
        foreach ($guiasDay as $guia) {
            $pesoHoy += $guia->peso;
        }

        $data = [
            'guias' => count($guias),
            'peso' =>$peso,
            'guiasDay' => count($guiasDay),
            'pesoHoy' => $pesoHoy,
            'manifiestos' => $manifiestos
        ];

        return $this->jsonResponse(
            true,
            "Guias Filtradas con exito",
            $data ,
            200
        );
    }

    public function consultar($guia)
    {
        $guia = Guia::where("id", $guia)
            ->with("trackings", "destinatario", "remitente", "empresa")
            ->first();
        if ($guia != null) {
            return $this->jsonResponse(
                true,
                "Guia obtenida con exito",
                $guia,
                200
            );
        } else {
            return $this->jsonResponse(false, "Guia no encontrada", null, 200);
        }
    }

    public function consultarescaneo($guia)
    {
        if (strlen($guia) == 5) {
            $guia = Guia::whereId($guia)
                ->where("escaneada", 0)
                ->with("empresa")
                ->first();
        }
        if (strlen($guia) == 10) {
            $guia = Guia::where("guiaZoom", $guia)
                ->where("escaneada", 0)
                ->with("empresa")
                ->first();
        }

        if ($guia != null) {
            return $this->jsonResponse(
                true,
                "Guia obtenida con exito",
                $guia,
                200
            );
        } else {
            return $this->jsonResponse(
                false,
                "Esta guia no pertenece a este manifiesto, o ya se encuentra escaneada!",
                null,
                200
            );
        }
    }

    public function guiasSinManifiesto(Request $q, $tipo)
    {
        $guias = Guia::query();
        $guias->where("idManifiesto", null);
        $guias->where("tipoGuia", $tipo);
        $guias->where("idUsuario", $q->user()->id);

        return $this->jsonResponse(true, $guias->count(), $guias->get(), 200);
    }

    public function zoom(Request $request)
    {
        $file = $request->file("file");
        if (is_file($file)) {
            $nombre = $this->upload_global($file, "excelszomm");
        }

        if (
            $xlsx = SimpleXLSX::parse(
                public_path() . "/uploads/excelszomm/" . $nombre
            )
        ) {
            $contador = count($xlsx->rows());
            $rows = $xlsx->rows();
            for ($i = 2; $i < $contador; $i++) {
                try {
                    $guia = explode("-", $rows[$i][5]);
                    $guia = Guia::find($guia[1]);
                    if ($guia != null) {
                        $guia->guiaZoom = $rows[$i][1];
                        $guia->save();
                    }
                } catch (\Throwable $th) {
                    return $rows[$i];
                }
            }
        }
        return $this->jsonResponse(
            true,
            "Archivo subido con exito",
            "Tramite",
            200
        );
    }

    public function filtros(Request $q)
    {
        if ($q->user()->typeUser == 1) {
            $oficinas = Sucursal::all();
            $clientes = Cliente::all();
            $usuarios = User::where("typeUser", 3)->get();
        }

        if ($q->user()->typeUser > 1) {
            $oficinas = Sucursal::where(
                "idEmpresa",
                $q->user()->idEmpresa
            )->get();
            $clientes = Cliente::where(
                "idEmpresa",
                $q->user()->idEmpresa
            )->get();
            $usuarios = User::where("typeUser", 3)
                ->where("idEmpresa", $q->user()->idEmpresa)
                ->get();
        }

        $data = [
            "oficinas" => $oficinas,
            "usuarios" => $usuarios,
            "clientes" => $clientes,
        ];

        return $this->jsonResponse(
            true,
            "Filtros obtenidos on exito",
            $data,
            200
        );
    }

    public function estaditicas($date = null, $empresa = null)
    {
        // return $date;
        $guias = Guia::query();
        if ($date == "null") {
            $date = date("Y-m-d");
        }

        $empresas = Empresa::withCount([
            "guias" => function ($query) use ($date) {
                $query->whereDate("created_at", $date);
            },
            "guias as peso" => function ($query) use ($date) {
                $query->whereDate("created_at", $date);
                $query->select(DB::raw("sum(peso)"));
            },
            "guias as volumen" => function ($query) use ($date) {
                $query->whereDate("created_at", $date);
                $query->select(DB::raw("sum(pesoVolumetrico)"));
            },
        ])
            ->orderBy("peso", "desc")
            ->get();
        $guias->whereDate("created_at", $date);

        $sucursales = "";
        if ($empresa != "undefined") {
            $guias->where("idEmpresa", $empresa);
            $empresas = Empresa::where("id", $empresa)
                ->withCount([
                    "guias" => function ($query) use ($date) {
                        $query->whereDate("created_at", $date);
                    },
                    "guias as peso" => function ($query) use ($date) {
                        $query->whereDate("created_at", $date);
                        $query->select(DB::raw("sum(peso)"));
                    },
                    "guias as volumen" => function ($query) use ($date) {
                        $query->whereDate("created_at", $date);
                        $query->select(DB::raw("sum(pesoVolumetrico)"));
                    },
                ])
                ->orderBy("peso", "desc")
                ->get();

            $sucursales = Sucursal::where("idEmpresa", $empresa)->get();
        }

        $guias = $guias->get();
        $peso = round($guias->sum("peso"), 3);
        $volumen = round($guias->sum("pesoVolumetrico"), 3);
        $conteo = $guias->count();
        return [
            "peso" => $peso,
            "volumen" => $volumen,
            "conteo" => $conteo,
            "empresas" => $empresas,
            "sucursales" => $sucursales,
        ];
    }

    public function estaditicasEmpresa($empresa)
    {
        // return $date;
        $guias = Guia::query();
        $guias->where("idEmpresa", $empresa);
        $sucursales = Sucursal::where("idEmpresa", $empresa)
            ->withCount([
                "guias",
                "guias as peso" => function ($query) {
                    $query->select(DB::raw("sum(peso)"));
                },
                "guias as volumen" => function ($query) {
                    $query->select(DB::raw("sum(pesoVolumetrico)"));
                },
            ])
            ->orderBy("peso", "desc")
            ->get();

        $guias = $guias->get();
        $peso = round($guias->sum("peso"), 3);
        $volumen = round($guias->sum("pesoVolumetrico"), 3);
        $conteo = $guias->count();
        return [
            "peso" => $peso,
            "volumen" => $volumen,
            "conteo" => $conteo,
            "sucursales" => $sucursales,
        ];
    }

    public function estaditicasFiltros(Request $q)
    {
        // return $date;
        $guias = Guia::query();
        $guias->where("idEmpresa", $q->empresa);
        if ($q->inicio && !$q->fin) {
            $guias->whereDate("created_at", $q->inicio);
        }
        if ($q->inicio && $q->fin) {
            $guias->whereBetween("created_at", [$q->inicio, $q->fin]);
        }

        $sucursales = Sucursal::where("idEmpresa", $q->empresa);

        $sucursales
            ->withCount([
                "guias as guias_count" => function ($query) use ($q) {
                    if ($q->inicio && !$q->fin) {
                        $query->whereDate("created_at", "=", $q->inicio);
                    }

                    if ($q->inicio && $q->fin) {
                        $query->whereBetween("created_at", [
                            $q->inicio,
                            $q->fin,
                        ]);
                    }
                },
                "guias as peso" => function ($query) use ($q) {
                    if ($q->inicio && !$q->fin) {
                        $query->whereDate("created_at", "=", $q->inicio);
                    }

                    if ($q->inicio && $q->fin) {
                        $query->whereBetween("created_at", [
                            $q->inicio,
                            $q->fin,
                        ]);
                    }
                    $query->select(DB::raw("sum(peso)"));
                },
                "guias as volumen" => function ($query) use ($q) {
                    if ($q->inicio && !$q->fin) {
                        $query->whereDate("created_at", "=", $q->inicio);
                    }

                    if ($q->inicio && $q->fin) {
                        $query->whereBetween("created_at", [
                            $q->inicio,
                            $q->fin,
                        ]);
                    }
                    $query->select(DB::raw("sum(pesoVolumetrico)"));
                },
            ])
            ->orderBy("peso", "desc");

        $resultadosSucursales = $sucursales->get();

        $guias = $guias->get();
        $peso = round($guias->sum("peso"), 3);
        $volumen = round($guias->sum("pesoVolumetrico"), 3);
        $conteo = $guias->count();
        return [
            "peso" => $peso,
            "volumen" => $volumen,
            "conteo" => $conteo,
            "sucursales" => $resultadosSucursales,
        ];
    }

    public function estaditicasGlobales()
    {
        // return $date;
        $guias = Guia::query();
        $empresas = Empresa::withCount([
            "guias",
            "guias as peso" => function ($query) {
                $query->select(DB::raw("sum(peso)"));
            },
            "guias as volumen" => function ($query) {
                $query->select(DB::raw("sum(pesoVolumetrico)"));
            },
        ])
            ->orderBy("peso", "desc")
            ->get();

        $volumen = Guia::whereDate("created_at", date("Y-m-d"))
            ->get()
            ->sum("peso");

        $guias = $guias->get();
        $peso = round($guias->sum("peso"), 3);
        // $volumen =round($guias->sum('pesoVolumetrico'), 3);
        $conteo = $guias->count();
        return [
            "peso" => round($peso, 3),
            "volumen" => round($volumen, 3),
            "conteo" => $conteo,
            "empresas" => $empresas,
        ];
    }

    public function updateScaneo(Guia $guia)
    {
        $guia->escaneada = 1;
        $guia->save();

        return $this->jsonResponse(
            true,
            "Guia Actualizada con exito",
            $guia,
            200
        );
    }

    public function eliminar(Guia $guia)
    {
        $guia->delete();
        return $this->jsonResponse(
            true,
            "Guia Eliminada con exito",
            $guia,
            200
        );
    }

    public function pruebaZoom($guia)
    {
        return $this->generateShipment($guia, 3);
    }

    public function upload_global($file, $folder)
    {
        $file_type = $file->getClientOriginalExtension();
        $destinationPath = public_path() . "/uploads/" . $folder;
        $destinationPathThumb = public_path() . "/uploads/" . $folder . "thumb";
        $filename =
            uniqid() . "_" . time() . "." . $file->getClientOriginalExtension();
        $url = "/uploads/" . $folder . "/" . $filename;

        if ($file->move($destinationPath . "/", $filename)) {
            return $filename;
        }
    }
}
