<?php

namespace App\Http\Controllers;

use App\Models\Carga;
use App\Models\Guia;
use App\Models\Manifiesto;
use App\Models\ManifiestoGuia;
use App\Models\ManifiestoTracking;
use App\Traits\JsonResponseTrait;
use App\Traits\TrackingGuiaTrait;
use Illuminate\Http\Request;

class ManifiestoController extends Controller
{
    use JsonResponseTrait;
    use TrackingGuiaTrait;
    public function listado(Request $q)
    {
        $manifiestos = Manifiesto::query();
        $manifiestos->with(['sucursal']);
        $manifiestos->withCount('guias');
        if ($q->user()->typeUser == 2) {$manifiestos->where('idEmpresa', $q->user()->idEmpresa);}
        if ($q->user()->typeUser == 3) {$manifiestos->where('idUsuario', $q->user()->id);}

        return $this->jsonResponse(true, 'Listado de Manifiestos', $manifiestos->orderByDesc('id')->get(), 200);
    }

    public function store(Request $request)
    {
        // crea manifiesto
        $manifiesto = Manifiesto::create([
            'idUsuario' => $request->user()->id,
            'idSucursal' => $request->user()->idSucursal,
            'idEmpresa' => $request->user()->idEmpresa,
            'tipoGuia' => $request->tipoGuia,
            'estado' => 'CREADO',
            'fecha' => date('Y-m-d'),
        ]);

        // recorre las guias y guarda las que tengan estado true
        $guias = $request->guias;
        foreach ($guias as $guia) {
            if ($guia['status'] == true) {
                ManifiestoGuia::create([
                    'idManifiesto' => $manifiesto->id,
                    'idGuia' => $guia['guia'],
                ]);

                $this->asignarManifiesto($manifiesto->id, $guia['guia']);
            }
        }
        // crea el tracking al manifiesto
        ManifiestoTracking::create([
            'idManifiesto' => $manifiesto->id,
            'estado' => 'Almacén',
        ]);

        // crea el tracking a cada guia
        foreach (ManifiestoGuia::where('idManifiesto', $manifiesto->id)->get() as $guia) {
            $this->asignarEstado($guia->idGuia, 'Almacén', null, null);
        }

        // da una respuesta
        return $this->jsonResponse(true, 'Manifiesto Creado con exito', $manifiesto, 200);
    }

    public function storeIntegradores(Request $request)
    {
        // crea manifiesto
        $manifiesto = Manifiesto::create([
            'idUsuario' => $request->user()->id,
            'idSucursal' => $request->user()->idSucursal,
            'idEmpresa' => $request->user()->idEmpresa,
            'tipoGuia' => $request->tipoGuia,
            'estado' => 'CREADO',
            'fecha' => date('Y-m-d'),
        ]);

        // recorre las guias y guarda las que tengan estado true
        $guias = $request->guias;
        foreach ($guias as $guia) {
            if ( isset($guia['status']) && $guia['status'] == true) {
                ManifiestoGuia::create([
                    'idManifiesto' => $manifiesto->id,
                    'idGuia' => $guia['id'],
                ]);

                $this->asignarManifiesto($manifiesto->id, $guia['id']);
            }
        }
        // crea el tracking al manifiesto
        ManifiestoTracking::create([
            'idManifiesto' => $manifiesto->id,
            'estado' => 'Almacén',
        ]);

        // crea el tracking a cada guia
        foreach (ManifiestoGuia::where('idManifiesto', $manifiesto->id)->get() as $guia) {
            $this->asignarEstado($guia->idGuia, 'Almacén', null, null);
        }

        // da una respuesta
        return $this->jsonResponse(true, 'Manifiesto Creado con exito', $manifiesto, 200);
    }

    public function edit(Request $q, Manifiesto $manifiesto)
    {
        $guiasManifiesto = Guia::where('idManifiesto', $manifiesto->id)->get();
        $guias = Guia::query();
        $guias->where('idManifiesto', null);
        $guias->where('tipoGuia', $manifiesto->tipoGuia);
        if ($q->user()->typeUser == 2) {$guias->where('idEmpresa', $q->user()->idEmpresa);}
        if ($q->user()->typeUser == 3) {$guias->where('idUsuario', $q->user()->id);}

        $guiasF = $guiasManifiesto->merge($guias->get());

        $data = [
            'manifiesto' => $manifiesto,
            'guias' => $guiasF,
        ];

        return $this->jsonResponse(
            true,
            'Guias Obtenidas',
            $data,
            200
        );
    }

    public function update(Request $request)
    {
        $guias = $request->guias;

        foreach ($guias as $guia) {
            if ($guia['status'] == true && $guia['manifiesto'] == 0) {
                ManifiestoGuia::create([
                    'idManifiesto' => $request->manifiesto,
                    'idGuia' => $guia['guia'],
                ]);

                $this->asignarManifiesto($request->manifiesto, $guia['guia']);
            }

            if ($guia['status'] == false && $guia['manifiesto'] != 0) {
                $rel = ManifiestoGuia::where('idGuia', $guia['guia'])->where('idManifiesto', $guia['manifiesto'])->delete();
                $this->asignarManifiesto(null, $guia['guia']);
                $this->retirarTracking($guia['guia']);
            }
        }
    }
    public function getSinCarga(Request $q, $tipo)
    {
        $manifiestos = Manifiesto::query();
        //$manifiestos->where('tipoGuia', $tipo);
        $manifiestos->where('idCarga', null);
        if ($q->user()->typeUser == 2) {$manifiestos->where('idEmpresa', $q->user()->idEmpresa);}
        if ($q->user()->typeUser == 3) {$manifiestos->where('idUsuario', $q->user()->id);}

        return $this->jsonResponse(true, 'Listado de Manifiestos', $manifiestos->get(), 200);
    }

    public function show($manifiesto)
    {
        $manifiesto = Manifiesto::where('id', $manifiesto)->withCount('guias')->with('guiasScaneadas')->first();
        if ($manifiesto != null) {
            return $this->jsonResponse(true, 'Manifiesto detalle', $manifiesto, 200);
        }

        return $this->jsonResponse(false, 'Manifisto no encontrado', null, 200);
    }

    public function eliminar(Manifiesto $manifiesto)
    {
        $guias = Guia::where('idManifiesto', $manifiesto->id)->get();
        foreach ($guias as $guia) {
            $guia->idManifiesto = null;
            $guia->save();
        }
        $manifiesto->delete();
        return $this->jsonResponse(false, 'Manifisto eliminado con exito', $manifiesto, 200);

    }

    public function addMasivoCarga(Manifiesto $manifiesto, Carga $carga)
    {
        $manifiesto->idCarga = $carga->id;
        $manifiesto->save();

        $guias = Guia::where('idManifiesto', $manifiesto->id)->get();
        foreach ($guias as $guia) {
            $guia->idCarga = $carga->id;
            $guia->save();
        }
    }
}
