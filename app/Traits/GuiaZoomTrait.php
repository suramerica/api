<?php

namespace App\Traits;

use App\Models\Guia;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Http;

trait GuiaZoomTrait
{
    private $login = "1";
    private $password = "456789";
    private $token = null;
    private $certificado = null;
    
    public function generateShipment($guia, $tipoEnvio)
    {
        $this->token = $this->generateToken();
        $this->certificado = $this->generateCert();

        $guia = Guia::where('id', $guia)->with(['destinatario', 'remitente', 'articulos'])->first();
        $productos = '';
        foreach($guia->articulos as $articulo)
        {
           $productos .= $articulo->producto . ', ';
        }
        $valorTotal = 0;
        foreach($guia->articulos as $articulo){
            $valor = $articulo->cantidad * $articulo->monto;
            $valorTotal = $valorTotal + $valor;
        }

        $data = [
            'login' => $this->login,
            'clave' => $this->password,
            'certificado' => $this->token,
            'codservicio' => 3,
            'remitente' => $guia->remitente->nombre.' '.$guia->remitente->apellido,
            'contacto_remitente' => $guia->remitente->nombre.' '.$guia->remitente->apellido,
            'telefono_remitente' => $guia->remitente->telefono,
            'direccion_remitente' => $guia->remitente->estado .' '.$guia->remitente->ciudad,
            'codpaisdes' => 124,
            'ciudaddes' => $guia->destinatario->ciudad,
            'retira_oficina' => $guia->entregaPP,
            'codoficinades' => $guia->idOficinaCourier,
            'destinatario' => $guia->destinatario->nombre.' '.$guia->destinatario->apellido,
            'contacto_destino' => $guia->destinatario->nombre.' '.$guia->destinatario->apellido,
            'rif_ci_destinatario' => $guia->destinatario->documento,
            'telefono_destino' => $guia->destinatario->telefono,
            'direcciondes' => $guia->destinatario->direccion,
            'tipo_envio' => "M",
            'numero_piezas' => 1,
            'peso_bruto' => $guia->peso,
            'valor_declarado' => $valorTotal,
            'alto' => $guia->alto,
            'ancho' => $guia->ancho,
            'largo' => $guia->largo,
            'descripcion_contenido' => substr($productos, 0, 75),
            'web_services' => 1,
            'seguro' => 1
        ];

        $response = Http::withHeaders([
                    'Accept' => '*/*',
                    'User-Agent' => 'Thunder Client (https://www.thunderclient.com)',
                ])
                ->post('https://qa.zoom.red/api/guiaelectronica/createShipmentInternacional', $data);
        $responseArray = json_decode($response, true);
        return $responseArray;
    }

    private function generateToken()
    {
        $client = new Client();
        $request = new Request('POST', 'http://sandbox.zoom.red/baaszoom/public/guiaelectronica/generarToken?login='.$this->login.'&clave='.$this->password);
        $res = $client->sendAsync($request)->wait();
        $result = $res->getBody();
        $responseArray = json_decode($result, true);
        $token = $responseArray['entidadRespuesta'][0]['token'];
        return $token;
    }

    private function generateCert()
    {
        $response = Http::withHeaders([ 
        'Accept'=> '*/*', 
        ]) 
        ->post('http://sandbox.zoom.red/baaszoom/public/guiaelectronica/zoomCert?login='.$this->login.'&password='.$this->password.'&token='.$this->token.'&frase_privada=RH0sVTL9za7O6gutqI43'); 

       $responseArray = json_decode($response, true);
        $token = $responseArray['entidadRespuesta'][0]['certificado'];
        return $token;
    }
}