<?php

namespace App\Traits;
use App\Models\BultosCargas;
use App\Models\Guia;
use App\Models\GuiaTracking;
use App\Models\ManifiestoGuia;
use App\Models\ManifiestosBulto;
use App\Models\PushNotificaciones;

trait TrackingGuiaTrait {
    use PushTrait;

    public function asignarEstado($guia, $estado, $oficina = null, $destino = null)
    {
        $guia = Guia::where('id', $guia)->with('destinatario')->first();
        
        // Cuando Se crea
        if($estado == 'Creacion') { $mensaje = "Paquete recepcionado en oficina ".$oficina; }
        // Cuando se genera el manifiesto
        if($estado == 'Almacén') { $mensaje = "Tu paquete fue recibido en nuestros almacenes / sedes."; }
        // Cuando se genera la carga
        if($estado == 'Aduana origen') { $mensaje = "Su paquete ha sido entregado a la aduana origen donde se le cargarán los aranceles de salida del país o aranceles. "; }
        // 1 dia despues
        if($estado == 'En Transito') { $mensaje = "Su paquete se encuentra en translado al destino."; }
        // 2 dias despues
        if($estado == 'Aduana destino') { $mensaje = "Su paquete ha llegado a la aduana en destino donde se le cargarán los aranceles de ingreso al país y será tramitado para el envío al Centro de Distribución"; }

        // Trackisn Manuales
        if($estado == 'Recibido en Distribución') { $mensaje = "El paquete se encuentra en el Centro de Distribución para su envío"; }

        if($estado == 'Transferido' ) {$mensaje = "Su paquete fue enviado a la sucursal de retiro.";}

        if($estado == 'Disponible') {$mensaje = " Ya puedes retirar tu paquete.";}
        
        if($estado == 'Entregado') {$mensaje = " Guia Entradada a ".$guia->destinatario->nombre . " ". $guia->destinatario->apellido ." el ". date('d-m-Y');}

        // Este estado es una exepcion que permite poner la carga en stop
        if($estado == 'Almacén ADT') { $mensaje = "Mercancía no despachada a la espera de recibir el transbordo hacia destino";}
        
        // Aqui ya agarra Zoom

        // Se genera el estado
        GuiaTracking::create([
            'idGuia' => $guia->id,
            'estado' => $estado,
            'descripcion' => $mensaje
        ]);

        // validar si existe un registro de notificacion
        $tokens = PushNotificaciones::where('idGuia', $guia->id)->get();

        foreach ($tokens as $token) {
            $tokenF = $token->token;
            $this->sendTracking($tokenF, 'Guia '.$guia->id.': '.$estado, $mensaje);
        }
        
        return true;
    }

    public function asignarManifiesto($manifiesto, $guia)
    {
        $guia = Guia::find($guia);
        $guia->idManifiesto = $manifiesto;
        $guia->save();

        return true;
    }

    public function calcularPesoBulto($bulto)
    {
        $peso = 0;
        $manifiestos = ManifiestosBulto::where('idBulto', $bulto)->get();
        foreach ($manifiestos as $manifiesto) {
            $guias = ManifiestoGuia::where('idManifiesto', $manifiesto->idManifiesto)->get();
            foreach ($guias as $guia) {
                $guia = Guia::find($guia->idGuia);
                $peso = $peso + $guia->peso;
            }
        }

        $bulto = BultosCargas::find($bulto);
        $bulto->peso = $peso;
        $bulto->save();
    
        return true;
    }

    public function retirarTracking($guia)
    {
        GuiaTracking::where('estado', 'Almacén')->where('id', $guia)->delete();
        return true;
    }
}