<?php

namespace App\Traits;

use sngrl\PhpFirebaseCloudMessaging\Client;
use sngrl\PhpFirebaseCloudMessaging\Message;
use sngrl\PhpFirebaseCloudMessaging\Recipient\Device;
use sngrl\PhpFirebaseCloudMessaging\Notification;

trait PushTrait
{
    public function sendPush($token, $mensaje)
    {
        $server_key = 'AAAA88mgfIk:APA91bHWTVubWGPCgyBJH6j9Iko44OLR371NjHtEbR_uX0Gis09q9N4nyxWU9A5PBrCS56oc_Coml7XNu0UE13EUjbLBOymFpttn4Yr0m5eaQsxNYJEEl80G-tgSHaWIRJ7z3pgI1MIp';
        $client = new Client();
        $client->setApiKey($server_key);
        $client->injectGuzzleHttpClient(new \GuzzleHttp\Client());

        $message = new Message();
        $message->setPriority('high');
        $message->addRecipient(new Device($token));
        $message
            ->setNotification(new Notification('Sur America Cargo', $mensaje))
            ->setData(['key' => 'value']);

        $response = $client->send($message);

        return $response;
    }

    public function sendTracking($token, $title, $mensaje)
    {
        $server_key = 'AAAA88mgfIk:APA91bHWTVubWGPCgyBJH6j9Iko44OLR371NjHtEbR_uX0Gis09q9N4nyxWU9A5PBrCS56oc_Coml7XNu0UE13EUjbLBOymFpttn4Yr0m5eaQsxNYJEEl80G-tgSHaWIRJ7z3pgI1MIp';
        $client = new Client();
        $client->setApiKey($server_key);
        $client->injectGuzzleHttpClient(new \GuzzleHttp\Client());

        $message = new Message();
        $message->setPriority('high');
        $message->addRecipient(new Device($token));
        $message
            ->setNotification(new Notification($title, $mensaje))
            ->setData(['key' => 'value']);

        $response = $client->send($message);

        return $response;
    }
}
