<?php

namespace App\Traits;

use App\Models\Carga;
use App\Models\CargaTracking;
use App\Models\Guia;

trait TrackingCargaTrait
{
    use TrackingGuiaTrait;
    public function asignarTrackingCarga($carga, $estado = null)
    {
            $carga = Carga::find($carga);

            if ($estado == 'Aduana origen') {
                $carga->et = true;
            }

            if ($estado == 'En Transito') {
                $carga->et = true;
            }
            // 2 dias despues
            if ($estado == 'Aduana destino') {
                $carga->add = true;
            }

            // Este estado es una exepcion que permite poner la carga en stop
            if ($estado == 'Almacén ADT') {
                $carga->adt = true;
            }

            CargaTracking::create([
                'idCarga' => $carga->id,
                'estado' => $estado
            ]);
            
            $carga->save();
    }
}