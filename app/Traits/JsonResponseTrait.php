<?php

namespace App\Traits;

trait JsonResponseTrait
{
    public function jsonResponse($status, $message, $data = null, $code = null, $token = null, $error = null)
    {
        $response = [
            'status' => $status,
            'message' => $message,
        ];

        if (!is_null($data)) {
            $response['data'] = $data;
        }
        if(!is_null($token)){
            $response['token'] = $token;
        }
        if(is_null($error)){
            $response['errors'] = $error;
        }

        return response()->json($response, $code);
    }
}
