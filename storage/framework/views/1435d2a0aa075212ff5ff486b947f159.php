<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Factura sur america</title>
    <style>
        * {
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif'

        }

        .table {
            width: 100%;
            border-collapse: collapse;
        }

        .table,
        .th,
        .td {
            border: 1px solid black;
        }

        th,
        td {
            padding: 5px;
            text-align: center;
        }
    </style>
</head>

<body>
    <table width="100%" border="0px" style="border: 0px">
        <tr width="100%" border="0px" style="border: 0px">
            <td style="width: 15%; border: 0px">
                <img src="https://sis.suramericacargo.com/img/logo.c2cc3f92.png" width="100%" class="logo"
                    alt="">
            </td>
            <td width="60%" style="border: 0px">
                <div style="font-weight: 900">SUR AMERICA CARGO</div>
                <span style="font-size: 9px !important; width: 100%">
                    PRINCIPAL CONSOLIDADOR DE ZOOM IMPORTACION - EXPORTACIONES <BR>
                    AV. MENDIOLA ALFREDO NRO. 3471 URB. PANAMERICANA NORTE - LIMA LIMA LOS OLIVOS<BR>
                </span>
            </td>
            <td width="30%" style="border: 0px">
                <div style="width: 100%; border: 1px solid black; border-radius: 10px">
                    <br>
                    <div
                        style="width: 100%; text-align: center; font-size: 24px; font-weight: bold; background-color: gray">
                        Consolidado Nº
                        CSL-<?php echo e($factura->id); ?>

                    </div>
                    <br>
                </div>
            </td>
        </tr>
    </table>

    <table width="100%" class="table" style="margin-top: 1rem;">
        <tr width="100%">
            <td width="20%" style="font-weight: 400; background-color: gray; color: white">
                EMPRESA
            </td>
            <td width="30%">
                <?php echo e($factura->empresa->nombre); ?>

            </td>
            <td width="10%" style="font-weight: 400; background-color: gray; color: white">
                FECHA
            </td>
            <td width="10%">
                <?php echo e($factura->fecha_emision); ?>

            </td>
        </tr>
        <tr width="100%" style="border-top: solid 1px black">
            <td width="20%" style="font-weight: 400; background-color: gray; color: white">
                REPRESENTANTE
            </td>
            <td width="30%">
                <?php echo e($factura->empresa->representante); ?>

            </td>
            <td width="10%" style="font-weight: 400; background-color: gray; color: white">
                TELEFONO
            </td>
            <td width="10%">
                <?php echo e($factura->empresa->telefono); ?>

            </td>
        </tr>
    </table>
    <table width="100%" style="margin-top: 2rem; border-radius: 2rem" class="table">
        <tr style="background-color: black; color: white; width: 100%">
            <td style="text-align: center; width: 5%">GUIA</td>
            <td style="text-align: center; width: 5%">TIPO DE COBRO</td>
            <td style="text-align: center; width: 5%">VALOR (kg)</td>
            <td style="text-align: center; width: 5%">MONTO (USD)</td>
        </tr>
        <?php $suma = 0; ?>
        <?php $__currentLoopData = $factura->guias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $guia): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr style="padding-top: 1rem; padding-bottom: 1rem; ">
                <td style="padding-top: 0.5rem; text-align: center; font-size: 12px"><?php echo e($guia->id); ?></td>
                <td style="padding-top: 0.5rem; text-align: center; font-size: 12px"><?php echo e($guia->tipo_cobro); ?></td>
                <td style="padding-top: 0.5rem; text-align: center; font-size: 12px"><?php echo e($guia->valor_cobrado); ?> KG</td>
                <td style="padding-top: 0.5rem; text-align: center; font-size: 12px"><?php echo e($guia->monto_cobrado); ?> USD</td>
                <?php $suma = $suma + $guia->monto_cobrado; ?>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <tr style="border-top: solid 1px black">
            <td colspan="3" style="text-align: right; font-weight: bold">Total</td>
            <td colspan="1"><?php echo e($suma); ?> USD</td>
        </tr>
    </table>

    <div style="font-size: 1rem">
        Cuentas de Pago:<br>
        Cta BBVA Soles <br>
        011-0712-02-00440596<br>
        CCI: 011-712-000200440596-71<br>
        Titular: Sur America Impo-Cargo E.I.R.L
        Cta. BBVA dolares<br>
        0011-0712-02-00444125<br>
        CCI: 011-712-000200444125-75<br>
        Pagos en *Zelle<br>
        Administration@zoom.red
    </div>
</body>

</html>
<?php /**PATH /Users/devarmandocampos/Documents/Proyectos/suramerica/api/resources/views/pdfs/factura.blade.php ENDPATH**/ ?>