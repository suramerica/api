<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BultosCargasController;
use App\Http\Controllers\CargaController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\CourierController;
use App\Http\Controllers\DestinatarioController;
use App\Http\Controllers\EmpaquetadoController;
use App\Http\Controllers\EmpresaController;
use App\Http\Controllers\GuiaController;
use App\Http\Controllers\ManifiestoController;
use App\Http\Controllers\PaisController;
use App\Http\Controllers\PDFController;
use App\Http\Controllers\PreRegistroController;
use App\Http\Controllers\PushNotificacionesController;
use App\Http\Controllers\SucursalController;
use App\Http\Controllers\TasaController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\FacturacionController;
use Illuminate\Support\Facades\Route;

Route::post("register", [AuthController::class, "create"]);
Route::post("login", [AuthController::class, "login"]);

Route::get("getAllCountries", [PaisController::class, "getAll"]);
Route::get("consultar-guia/{guia}", [GuiaController::class, "consultar"]);
Route::get("consultar-guia-escaneo/{guia}", [
    GuiaController::class,
    "consultarescaneo",
]);
Route::get("paisesActivos", [PaisController::class, "activos"]);
Route::resource("paises", App\Http\Controllers\PaisController::class)->only([
    "index",
]);
Route::middleware(["auth:sanctum"])->group(function () {
    Route::resource(
        "empresas",
        App\Http\Controllers\EmpresaController::class
    )->only(["index", "store", "show"]);
    Route::resource(
        "sucursales",
        App\Http\Controllers\SucursalController::class
    )->only(["store", "show", "destroy"]);
    Route::resource("tasas", App\Http\Controllers\TasaController::class)->only([
        "store",
        "show",
    ]);
    Route::post("tasas/{tasa}", [TasaController::class, "update"]);
    Route::get("getTasa/{tasa}", [TasaController::class, "show"]);
    Route::get("tasas/byEmpresa/{empresa}", [
        App\Http\Controllers\TasaController::class,
        "getByEmpresa",
    ]);
    Route::get("tasas/byEmpresaGuia/{empresa}/{tipo}", [
        App\Http\Controllers\TasaController::class,
        "getByGuia",
    ]);
    Route::get("tasas/byEmpresaCalculadora/{empresa}", [
        App\Http\Controllers\TasaController::class,
        "getByCalculadora",
    ]);
    Route::post("sucursales/{sucursale}", [
        SucursalController::class,
        "update",
    ]);
    Route::post("paises/{paise}", [PaisController::class, "update"]);
    Route::post("empresas/{empresa}", [EmpresaController::class, "update"]);
    Route::post("updateTasa", [EmpresaController::class, "tasa"]);
    Route::post("empresas/{empresa}/status/{estado}", [
        EmpresaController::class,
        "status",
    ]);
    Route::get("user/{usuario}", [UserController::class, "show"]);
    Route::get("user/{usuario}/status/{estado}", [
        UserController::class,
        "status",
    ]);
    Route::get("usuarios/byEmpresa/{empresa}", [
        UserController::class,
        "getByEmpresa",
    ]);
    Route::post("user/update/{usuario}", [AuthController::class, "update"]);
    Route::get("logout", [AuthController::class, "logout"]);

    // Clientes
    Route::get("getCliente/{documento}", [
        ClienteController::class,
        "getCliente",
    ]);
    Route::post("clientes", [ClienteController::class, "store"]);
    Route::get("destinatarios", [DestinatarioController::class, "index"]);
    Route::get("getDestinatariosCliente/{cliente}", [
        DestinatarioController::class,
        "getByCliente",
    ]);

    // Guia
    Route::get("guias", [GuiaController::class, "listado"]);
    Route::post("guias", [GuiaController::class, "filtrada"]);
    Route::post("guias/fechas", [GuiaController::class, "filtrosfechas"]);

    Route::post("/guias/generate", [GuiaController::class, "generate"]);
    Route::post("/guias/generateIntegradores", [
        GuiaController::class,
        "generate",
    ]);
    Route::post("/guias/articulos", [GuiaController::class, "articulos"]);
    Route::get("/guias/sin-manifiestos/{tipo}", [
        GuiaController::class,
        "guiasSinManifiesto",
    ]);

    // Manifiestos
    Route::get("manifiestos", [ManifiestoController::class, "listado"]);
    Route::get("eliminarManifiesto/{manifiesto}", [
        ManifiestoController::class,
        "eliminar",
    ]);
    Route::post("manifiestos", [ManifiestoController::class, "store"]);
    Route::get("manifiestos/edit/{manifiesto}", [
        ManifiestoController::class,
        "edit",
    ]);
    Route::post("manifiestos/update", [ManifiestoController::class, "update"]);
    Route::get("manifiestos/sin-carga/{tipo}", [
        ManifiestoController::class,
        "getSinCarga",
    ]);
    Route::get("addMasivoCarga/{manifiesto}/{carga}", [
        ManifiestoController::class,
        "addMasivoCarga",
    ]);
    Route::post('integradores/manifiestos', [ManifiestoController::class, 'storeIntegradores']);

    // cargas
    Route::post("cargas", [CargaController::class, "store"]);
    Route::get("eliminarCarga/{carga}", [CargaController::class, "eliminar"]);
    Route::get("cargas", [CargaController::class, "index"]);
    Route::get("cargas/cerrar/{carga}", [CargaController::class, "cerrar"]);
    Route::get("getTrackings/carga/{carga}", [
        CargaController::class,
        "getTrackings",
    ]);
    Route::get("addTracking/carga/{carga}/{estado}", [
        CargaController::class,
        "trackingEstado",
    ]);

    // bultos
    Route::post("bultos", [BultosCargasController::class, "store"]);
    Route::get("cargas/escaneadas/{carga}", [
        BultosCargasController::class,
        "get",
    ]);
    Route::get("deleteGuiaCarga/{guia}", [
        BultosCargasController::class,
        "delete",
    ]);
    Route::post("subirtarchivo", [GuiaController::class, "zoom"]);

    // Clientes
    Route::get("clientes", [ClienteController::class, "index"]);
    Route::post("clientes/update/{cliente}", [
        ClienteController::class,
        "update",
    ]);
    Route::post("users/terminos/{ip}", [AuthController::class, "terminos"]);
    Route::post("destinatarios/update/{cliente}", [
        DestinatarioController::class,
        "update",
    ]);

    // OFicinas
    Route::post("couriers", [CourierController::class, "store"]);
    Route::get("filtros", [GuiaController::class, "filtros"]);
    Route::get("filterByAddres/{address}/{estado}", [
        CourierController::class,
        "filterByAddres",
    ]);
    Route::get("detalleCarga/{carga}", [CargaController::class, "detalle"]);

    Route::post("addGuiaCarga", [CargaController::class, "addGuia"]);

    Route::resource("empaquetado", EmpaquetadoController::class);


    // preregistros
    Route::get("pre-registros", [PreRegistroController::class, "index"]);

    // estadisticas
    Route::get("estadisticas/globales", [
        GuiaController::class,
        "estaditicasGlobales",
    ]);
    Route::get("estadisticas/empresa/{empresa}", [
        GuiaController::class,
        "estaditicasEmpresa",
    ]);
    Route::post("estadisticas/filtros", [
        GuiaController::class,
        "estaditicasFiltros",
    ]);

    Route::post("estadisticas/home/new", [
        GuiaController::class,
        "estadisticasHomeNew",
    ]);

    // Emails
    Route::get("email/welcome/{empresa}", [
        EmpresaController::class,
        "welcome",
    ]);
});
Route::get("couriers/generate", [CourierController::class, "generate"]);
// cronjobs
Route::get("actualizarTrackingCarga", [
    CargaController::class,
    "CronTrackingCarga",
]);

// PDFS
Route::get("excels/generar/manifiesto/{manifiesto}/{tipo}", [
    PDFController::class,
    "excelManifiesto",
]);
Route::get("excels/generar/manifiesto-zoom/{manifiesto}/{tipo}", [
    PDFController::class,
    "excelZoomManifiesto",
]);
Route::get("excels/generar/carga/{carga}", [
    PDFController::class,
    "excelCarga",
]);
Route::get("excels/generar/carga/{carga}/{tipo}", [
    PDFController::class,
    "excelZoomCarga",
]);
// Route::get('excels/previsualizar/carga/{carga}', [PDFController::class, 'previewCarga']);
Route::get("pdfs/generar/ticket/{guia}", [PDFController::class, "ticketGuia"]);
Route::get("pdfs/generar/manifiesto/{manifiesto}", [
    PDFController::class,
    "manifiestoPdf",
]);
Route::get("pdfs/generar/guia/{guia}", [PDFController::class, "guiaGuia"]);
Route::get("pdfs/generar/etiqueta/{guia}", [
    PDFController::class,
    "guiaEtiqueta",
]);
Route::resource("facturacion", FacturacionController::class);
Route::get("pdfs/generar/factura/{factura}", [
    PDFController::class,
    "facturaEmpresa",
]);
Route::get("pdfs/generar/parkinglist/{guia}", [
    PDFController::class,
    "parkingGuia",
]);

// Pregistros
Route::get("pre-registro/getDocument/{documento}/{tipoDocumento}", [
    PreRegistroController::class,
    "getDocument",
]);
Route::post("pre-registro/storeDocument/", [ClienteController::class, "store"]);
Route::get("getDestinatario/{documento}", [
    DestinatarioController::class,
    "getDestinatario",
]);
Route::get("couriers", [CourierController::class, "index"]);
Route::post("destinatarios", [DestinatarioController::class, "store"]);
Route::get("sucursales/byEmpresa/{empresa}", [
    SucursalController::class,
    "getByEmpresa",
]);
Route::get("sucursales/byEmpresa/web/{empresa}", [
    SucursalController::class,
    "getByEmpresaWeb",
]);
Route::post("pre-registro", [PreRegistroController::class, "store"]);
Route::get("getManifiesto/{manifiesto}", [ManifiestoController::class, "show"]);
Route::get("updateScaneo/{guia}", [GuiaController::class, "updateScaneo"]);

// notificaciones push
Route::post("/push/generate", [PushNotificacionesController::class, "store"]);
Route::get("eliminarGuia/{guia}", [GuiaController::class, "eliminar"]);

Route::get("prueba/{guia}", [GuiaController::class, "pruebaZoom"]);

// integraciones

Route::group(["prefix" => "integraciones"], function () {
    Route::get("getCliente/{documento}", [
        ClienteController::class,
        "getCliente",
    ]);
    Route::get("getDestinatario/{documento}", [
        DestinatarioController::class,
        "getDestinatario",
    ]);
    Route::get("couriers", [CourierController::class, "index"]);
    Route::get("tasas/byEmpresa/{empresa}", [
        App\Http\Controllers\TasaController::class,
        "getByEmpresa",
    ]);
    Route::get("empaquetado/byEmpresa/{empresa}", [
        EmpaquetadoController::class,
        "getByEmpresa",
    ]);
    Route::post("storeCliente/", [ClienteController::class, "store"]);
    Route::post("storeDestinatario", [DestinatarioController::class, "store"]);
    Route::middleware(["auth:sanctum"])->group(function () {
        Route::post("generar", [GuiaController::class, "generateIntegracion"]);
    });
});
